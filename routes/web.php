<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




 Route::get('list/{category?}','FrontController@home')->name('fronts.list');
Route::get('/hometab','FrontController@hometab');
Route::resource('/','DetailController');
Route::resource('/details','DetailController');
Route::resource('/agents', 'AgentController');
Route::group(['middlewareGroup' => 'webGroup'],function(){
Auth::routes();
// });
// Route::get('/','IndexController@getindex');
// Route::get('/details/test','DetailController@test');
Route::group(['middleware'=>'auth'],function(){
 Route::get('/blank','YieldController@yield');
 

});
});

Route::get('/home', 'HomeController@index')->name('jsontest');
