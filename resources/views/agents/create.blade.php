@extends('blank')
@include('agents.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('employees') }}">Employee Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('agents') }}">View All Agents</a></li>
        <li><a href="{{ URL::to('agents/create') }}">Create an Agent</a>
    </ul>
</nav>

<h1>Create an Agent</h1>

 @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @if(session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

<!-- if there are creation errors, they will show here -->
<form class="form-horizontal" enctype="multipart/form-data" method="post" action="/agents">
  {{csrf_field()}}


    <div>Add Agents</div>
<div class="form-group">
    <label for="id" class="col-sm-2 control-label">id</label>
  <div class="col-sm-10">
      <input type="id" class="form-control" id="id" placeholder="Agents">
    </div>
  </div>



  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Agent Name</label>
    <div class="col-sm-10">
      <input required type="text" class="form-control" name="name" placeholder="eg:suzan shrestha">
    </div>
  </div>
      
  <div class="form-group">
    <label for="phone" class="col-sm-2 control-label">Phone</label>
    <div class="col-sm-10">
      <input required type="int" class="form-control" name="phone" placeholder="9800000000">
    </div>
  </div>
  <div class="form-group">
    <label for="address" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
      <input required type="text" class="form-control" name="address" placeholder="Address">
    </div>
  </div>

  <div class="form-group">
    <label for="images" class="col-sm-2 control-label">Image</label>
    <div class="col-sm-10">
      <input required type="file" class="form-control" name="images" placeholder="Image">
    </div>
  </div>
  

    <div class="col-sm-offset-2 col-sm-10">
      <input required type="submit" name="submit" class="btn btn-primary">
    </div>

</form>
</div>
@endsection