<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Rentma</title>
    <link type="text/css" rel="stylesheet" href="../Cinqsnipelte/css/materialize.min.css" >
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/hover.css">   
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/style.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <style type="text/css">
    
        
    </style>
</head>
<body>
		
	<ul id="dropdown1" class="dropdown-content">
		<li><a href="#!">Home</a></li>
		<li><a href="#!">Apartment</a></li>			
		<li><a href="#!">Flat/Single Room</a></li>
		<li><a href="#!">Shop/Shutter</a></li>
		<li><a href="#!">Land</a></li>
	</ul>

	<ul id="dropdown2" class="dropdown-content">
		<li><a href="#!">Home</a></li>
		<li><a href="#!">Apartment</a></li>			
		<li><a href="#!">Flat/Single Room</a></li>
		<li><a href="#!">Shop/Shutter</a></li>
		<li><a href="#!">Land</a></li>
	</ul>

	<nav>
		<div class="container-fluid">
		    <div class="nav-wrapper">
		    	<a href="/" class="brand-logo"><img src="../Cinqsnipelte/images/logo1.png"></a>
		    	<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="fa fa-bars"></i></a>	      

		    	<ul class="right right-nav ">
		      		<div class="hide-on-med-and-down">
				        <li><a href="/details"  class="dropdown-button" data-activates="dropdown2">For Tenants <span><i class="fa fa-chevron-down"></i></span></a></li>
				        <li><a href="/agents"  class="dropdown-button" data-activates="dropdown1">For Landlord <span><i class="fa fa-chevron-down"></i></span></a></li>
				        <li><a href="/agents">Agents</a></li>	        	      
				        <a href="/details/create" class="waves-effect waves-light btn">Post Now</a>
		        	</div>
		        	<a class="waves-effect waves-light btn login modal-trigger" data-target="modal1" id="login-btn">Login/Sign Up</a>
		    	</ul>

		    	<ul class="side-nav menu" id="mobile-demo">
			        <li id="tenants-drop"><a>For Tenants <span><i class="fa fa-chevron-down"></i></span></a></li>
			        <div class="dropdown-tenants active">
			        	<li><a href="#">Home</a></li>
			        	<li><a href="#">Apartment</a></li>
			        	<li><a href="#">Flat/Single Room</a></li>
			        	<li><a href="#">Shop/Shutter</a></li>
			        	<li><a href="#">Land</a></li>
			        </div>
			        <li id="landlord-drop"><a>For Landlord <span><i class="fa fa-chevron-down"></i></span></a></li>
			        <div class="dropdown-landlord active">
			        	<li><a href="#">Home</a></li>
			        	<li><a href="#">Apartment</a></li>
			        	<li><a href="#">Flat/Single Room</a></li>
			        	<li><a href="#">Shop/Shutter</a></li>
			        	<li><a href="#">Land</a></li>
			        </div>
			        <li><a href="agents.html">Agents</a></li>	
			        <a href="/details/create" class="waves-effect waves-light btn">Post Now</a>		       
		    	</ul>

		        <div class="user">
			      	<a href="#" data-activates="mobile" class="btn-collapse"><img src="../Cinqsnipelte/images/card.png" class="circle"><span class="hvr-underline-from-center">Suman Jung</span></a>
			      	<ul class="side-nav " id="mobile">
			      		<div class="user-logo">				      			
			      			<img src="../Cinqsnipelte/images/card.png" class="circle">
			      			<div>
			      				<p>Suman Jung</p>
			      				<a class="btn" id="edit-btn">Edit <i class="fa fa-pencil"></i></a>
			      			</div>		      			
			      		</div>
			      		<div class="clearfix"></div>
				        <li class="menu-list">
				        	<a class="menu-btn" href="notification.html" data-hide="true">
				        		<img src="../Cinqsnipelte/images/notification-icon.png">
				        		<span class="notifier"></span>
				        		<span class="name">Notifications</span>
				        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
				        	</a>
				        </li>
				        <li class="menu-list">
				        	<a class="menu-btn" href="profile.html" data-hide="false">
				        		<img src="../Cinqsnipelte/images/profile-icon.png">
				        		<span class="name">My Profile</span>
				        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
				        	</a>
				        </li>
				        <li class="menu-list">
				        	<a class="menu-btn" href="mypost.html" data-hide="true">
				        		<img src="../Cinqsnipelte/images/post-icon.png">
				        		<span class="name">My Posts</span>
				        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
				        	</a>
				        </li>
				        <li class="menu-list">
				        	<a class="menu-btn" href="message.html" data-hide="true">
				        		<img src="../Cinqsnipelte/images/message-icon.png">
				        		<span class="notifier"></span>
				        		<span class="name">Messages</span>
				        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
				        	</a>
				        </li>
				         <li class="menu-list">
				        	<a class="menu-btn" href="favourites.html" data-hide="true">
				        		<img src="../Cinqsnipelte/images/favourite-icon.png">
				        		<span class="name">Favourites</span>
				        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
				        	</a>
				        </li>
				         <li class="menu-list">
				        	<a href="#!">
				        		<img src="../Cinqsnipelte/images/logout-icon.png">
				        		<span class="name">Logout</span>
				        		
				        	</a>
				        </li>
				    	<button class="close"><i class="fa fa-chevron-left"></i></button>
				    	<div id="content-here"></div>
			      	</ul>
			    </div><!--user ends-->

		    </div>
		</div>
	</nav>

	<div id="modal1" class="modal">
	    <div class="modal-content">
		    <div class="container">
		    <div class="row">
		    	<a data-status="on" class="modal-action modal-close"><i class="fa fa-times"></i></a>
		    	<div class="logo">
		    		<img src="../Cinqsnipelte/images/logo1.png">
		    	</div>
		    	<div class="login-page">
			      	<div class="acc">
			      		<div class="account col l8 m8 s8">
			      			<p>Don't have the account?</p>
			      		</div>
			      		<div class="signup col l4 m4 s4">
			      			<a class="waves-effect waves-light btn" id="signup">Sign Up</a>
			      		</div>
			      	</div>
			      	<div class="clearfix"></div>
			      	<div class="form-field">
			      		<form>
			      			<input type="text" placeholder="Username / Email">
			      			<input type="password" placeholder="Password">
			      		</form>
			      		<a class="waves-effect waves-light btn modal-action modal-close" id="modal-login">LOGIN</a>
			      	</div>
			      	<div class="or">
			      		<div class="line">
			      			<p>OR</p>
			      		</div>			      		
			      	</div>
			      	<div class="social-sites">
			      		<a href=""><img src="../Cinqsnipelte/images/facebook.png"></a>
			      		<a href=""><img src="../Cinqsnipelte/images/twitter.png"></a>
			      		<a href=""><img src="../Cinqsnipelte/images/google+.png"></a>
			      	</div>
		      	</div><!--login-page ends-->
		      	<div class="signup-page">
		      		<a class="back"><i class="fa fa-chevron-left"></i></a>
		      		<form>
		      			<input type="text" placeholder="Full Name">
		      			<input type="text" placeholder="Username">
		      			<input type="email" placeholder="abc@abc.com" class="validate">
		      			<input type="password" placeholder="Password">
		      			<input type="password" placeholder="Confirm Password">
		      			<input type="number" placeholder="Phone Number">
		      		</form>
		      		<a class="waves-effect waves-light btn">Sign Up</a>
		      	</div>			      
    		</div>
    		</div>
		</div>
	</div><!--modal ends here-->


	<div class="agentDetails-content">
    	<div class="agent-details">
    		<div class="row">
    			<div class="banner col l12 s12 m12">
    				<div class="container">    				
    					<h4 class="main-title">Agents</h4>
    				</div>   				
    			</div>
    			<div class="container" id="agentDetails-container">    				
    				<div class="details-section col l9 m12 s12">
    					<div class="normal-agents col l12 m12 s12">		
	    					<div class="agent-profile col l6 m6 s12">
	    						<div class="inner-wrap">					
	    							<div class="profile-wrap">
	    								<div class="profile-image">
	    									<a href=""><img src="/agentspic "></a>
	    								</div>
	    								<div class="agent-info">
	    									<a href=""><span class="name">{{$data->name}}</span></a>
	    									<p class="address">{{$data->address}} </p>
	    									<p class="company-name">Cinqsnipe Technology</p>
	    									<p class="contact">Call <span>{{$data->phone}}</span></p>
	    								</div>
	    							</div>		    						
	    						</div>
	    						
	    					</div><!--agent-profile ends-->
	    					<div class="agent-rating col l6 m6 s12">
	    						<p class="title-head">Ratings</p>
	    						<fieldset class="rating">
	    						
								    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
								    
								    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
								    
								    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
								    
								    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
								    
								    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
								   
								</fieldset>
								<div class="clearfix"></div>
								<a href="!#" class="waves-effect waves-light btn contact-btn">Contact</a>
	    					</div><!--agent-rating ends-->					
    					</div><!--normal-agent ends-->

    					<div class="agents-info col l12 m12 s12">
    						<div class="row">
							    <div class="col s12">
							      <ul class="tabs">
							        <li class="tab col s3"><a  href="#test1">Agent Info</a></li>
							        <li class="tab col s3"><a href="#test2">Listings for Sale & Rent</a></li>		        	        
							        <li class="tab col s3"><a href="#test4 " class="active">Reviews</a></li>
							        <li class="tab col s3"><a  href="#test3">Contact Agent</a></li>	
							      </ul>
							    </div>

							    <div class="info-details col s12 l12 m12">
								    <div id="test1" class="col s12">
								    	<div class="info-wrapper">    	
									    	<p class="intro">Introduction</p>
									    	<p class="intro-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
									    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
									    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									    	<p class="intro">Specialities & Services </p>
									    	<div class="inner-wrapper col l12 s12 m12">
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>Apartmental Rental</span></p>
									    	</div>
									    	<p class="intro">Regions Covered </p>
									    	<div class="inner-wrapper col l12 s12 m12">
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    		<p class="inner col l4 m6 s12"><span><i class="fa fa-caret-right"></i>City & South West (D01-08)</span></p>
									    	</div>
									    </div>

								    </div><!--test1 ends-->

								    <div id="test2" class="col s12">
								    	<div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="#" class="main-box">
													<div class="display-image">
											      		<img src="../Cinqsnipelte/build/images/house.png"/>
											      		<span class="caption">Chainpur, Bara</span>
											      	</div>
											      	<div class="card-info">					      	
												      	<p class="cost">Nrs 22000/mo. </p>
												      	<p class="need">Subhida yukta flat bhadama chaiyeko awastha </p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
										</div>

										<div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="#" class="main-box">
													<div class="display-image">
											      		<img src="../Cinqsnipelte/build/images/house.png"/>
											      		<span class="caption">Chainpur, Bara</span>
											      	</div>
											      	<div class="card-info">					      	
												      	<p class="cost">Nrs 22000/mo. </p>
												      	<p class="need">Subhida yukta flat bhadama chaiyeko awastha </p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
										</div>

										<div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="#" class="main-box">
													<div class="display-image">
											      		<img src="../Cinqsnipelte/build/images/house.png"/>
											      		<span class="caption">Chainpur, Bara</span>
											      	</div>
											      	<div class="card-info">					      	
												      	<p class="cost">Nrs 22000/mo. </p>
												      	<p class="need">Subhida yukta flat bhadama chaiyeko awastha </p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
										</div>

										<div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="#" class="main-box">
													<div class="display-image">
											      		<img src="../Cinqsnipelte/build/images/house.png"/>
											      		<span class="caption">Chainpur, Bara</span>
											      	</div>
											      	<div class="card-info">					      	
												      	<p class="cost">Nrs 22000/mo. </p>
												      	<p class="need">Subhida yukta flat bhadama chaiyeko awastha </p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
										</div>

										<div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="#" class="main-box">
													<div class="display-image">
											      		<img src="../Cinqsnipelte/build/images/house.png"/>
											      		<span class="caption">Chainpur, Bara</span>
											      	</div>
											      	<div class="card-info">					      	
												      	<p class="cost">Nrs 22000/mo. </p>
												      	<p class="need">Subhida yukta flat bhadama chaiyeko awastha </p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
										</div>

										<div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="#" class="main-box">
													<div class="display-image">
											      		<img src="../Cinqsnipelte/build/images/house.png"/>
											      		<span class="caption">Chainpur, Bara</span>
											      	</div>
											      	<div class="card-info">					      	
												      	<p class="cost">Nrs 22000/mo. </p>
												      	<p class="need">Subhida yukta flat bhadama chaiyeko awastha </p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
										</div>

										<div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="#" class="main-box">
													<div class="display-image">
											      		<img src="../Cinqsnipelte/build/images/house.png"/>
											      		<span class="caption">Chainpur, Bara</span>
											      	</div>
											      	<div class="card-info">					      	
												      	<p class="cost">Nrs 22000/mo. </p>
												      	<p class="need">Subhida yukta flat bhadama chaiyeko awastha </p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
										</div>
								    </div><!--test2 ends-->

								    <div id="test3" class="col s12">		    	
								    	<form>
								    		<div class="want">
								    			<p>I want agent to</p>
									    		<div class="input-field col s12">
												    <select>				      
												      <option value="1">Buy a property.</option>
												      <option value="2">Sell a property.</option>
												      <option value="3">Rent a property.</option>
												    </select>	
									    		</div>						    
										  	</div>
										  	<div class="clearfix"></div>
										  	<div class="want">
								    			<p>Choose Property Type You Need</p>
									    		<div class="input-field col s12">
												    <select>					      
												      <option value="1">Home</option>
												      <option value="2">Apartment</option>
												      <option value="3">Flat/Single Room</option>
												      <option value="4">Shop/Shutter</option>
												      <option value="3">Land</option>
												    </select>	
									    		</div>						    
										  	</div>
										  	<div class="clearfix"></div>
										  	<div class="want">
								    			<p>Full Name</p>
								    			<div class="input-field col s12">
									    			<input type="text" placeholder="Full Name">
									    		</div>					    
										  	</div>

										  	<div class="clearfix"></div>
										  	<div class="want">
								    			<p>Contact Number (Mobile Preferred)</p>
								    			<div class="input-field col s12">
									    			<input type="number" placeholder="Contact Number">
									    		</div>					    
										  	</div>

										  	<div class="clearfix"></div>
										  	<div class="want">
								    			<p>Email</p>
								    			<div class="input-field col s12">
									    			<input type="email" placeholder="abc@abc.com" class="validate">
									    		</div>					    
										  	</div>

										  	<div class="clearfix"></div>
										  	<div class="want">
								    			<p>Additional Queries</p>
								    			<div class="input-field col s12">
									    			<textarea placeholder="Additional Queries Here"></textarea>
									    		</div>					    
										  	</div>

										  	<button type="submit" class="waves-effect waves-light btn">Send Message</button>
								    	</form>
								    </div><!--test3 ends--> 

								    <div id="test4" class="col s12">
								    	<div class="review-wrapper col s12">
								    		<div class="user-avatar col l1 m2 s12">
								    			<div class="img-wrapper">
								    				<img src="../Cinqsnipelte/images/card.png">
								    			</div>
								    		</div>
								    		<div class="review-content col l10 m10 s12">
								    			<p class="users-name">John Doe</p>
								    			<fieldset class="rating">
						    						<p >Rate User</p>
													    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
													    
													    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
													    
													    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
													    
													    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
													    
													    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>			   
													</fieldset>
													<div class="clearfix"></div>
													<p class="user-review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
													tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
													quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
													consequat. </p>
								    		</div>
								    	</div><!--review-wrapper ends-->
								    	<div class="review-wrapper col s12">
								    		<div class="user-avatar col l1 m2 s12">
								    			<div class="img-wrapper">
								    				<img src="../Cinqsnipelte/images/card.png">
								    			</div>
								    		</div>
								    		<div class="review-content col l10 m10 s12">
								    			<p class="users-name">John Doe</p>
								    			<fieldset class="rating">
						    						<p >Rate User</p>
													    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
													    
													    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
													    
													    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
													    
													    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
													    
													    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>			   
													</fieldset>
													<div class="clearfix"></div>
													<p class="user-review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
													tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
													quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
													consequat. </p>
								    		</div>
								    	</div><!--review-wrapper ends-->
								    	<div class="review-wrapper col s12">
								    		<div class="user-avatar col l1 m2 s12">
								    			<div class="img-wrapper">
								    				<img src="../Cinqsnipelte/images/card.png">
								    			</div>
								    		</div>
								    		<div class="review-content col l10 m10 s12">
								    			<p class="users-name">John Doe</p>
								    			<fieldset class="rating">
						    						<p >Rate User</p>
													    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
													    
													    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
													    
													    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
													    
													    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
													    
													    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>			   
													</fieldset>
													<div class="clearfix"></div>
													<p class="user-review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
													tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
													quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
													consequat. </p>
								    		</div>
								    	</div><!--review-wrapper ends-->
								    </div><!--test4 ends-->
								</div><!--info-details ends-->
							  </div><!--row ends-->
    					</div><!--agents-info ends-->


						<div class="preloader-wrapper big active">
						    <div class="spinner-layer spinner-blue-only">
        				        <div class="circle-clipper left">
								    <div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper">
								    <div class="circle"></div>
							    </div>
							</div>
						</div>	 		
						
	    			</div><!--details-section ends-->

	    			<div class="variety col l3 m12 s12">    				
		    				
    					<div class="adv col l12 m6 s12">
    						<img src="../Cinqsnipelte/images/turbine.png">
    					</div><!--adv ends-->   				

    					<div class="similar col l12 m6 s12">
    						<div class="top">
    							<h5>Sponsored Property</h5>    							
    							<div class="propic">
    								<img src="../Cinqsnipelte/images/propic.png" class="circle">
    								<p>By <span>Suman Jung</span></p>
    							</div>
    						</div>
    						<div class="image">
    							<img src="../Cinqsnipelte/images/similar.png">
    						</div>
    						<div class="bottom">
    							<h5>Subhida Yukta Ghar Bhadama</h5>
    							<h4>NRP 15000</h4>
    							<p><i class="fa fa-map-marker"></i> Koila Galli, Wotu, New road, Kathmandu</p>
    						</div>
    						<a class="waves-effect waves-light btn sponsor">Sponsor</a>
    					</div><!--similar ends-->  	

					</div><!--variety ends-->
    			</div><!--container ends-->
    		</div><!--row ends-->
    	</div><!--details ends-->    	

		<div class="list-total">
			<div class="row">
				<div class="container">
					<div class="list col m3 l3 s12">
						<img src="../Cinqsnipelte/build/images/home.png" alt="" />
						<h4>1052</h4>
						<p>Property Listed</p>
					</div>
					<div class="list col m3 l3 s12">
						<img src="../Cinqsnipelte/build/images/apartment.png" alt="" />
						<h4>3956</h4>
						<p>User Registered</p>
					</div>
					<div class="list col m3 l3 s12">
						<img src="../Cinqsnipelte/build/images/flat.png" alt="" />
						<h4>568</h4>
						<p>Property Rented</p>
					</div>
					<div class="list col m3 l3 s12">
						<img src="../Cinqsnipelte/build/images/shop.png" alt="" />
						<h4>1052</h4>
						<p>Project Listed</p>
					</div>			
				</div>
			</div>
		</div><!--total ends-->
	</div><!--list-content ends-->

	<footer class="page-footer">
        <div class="container">
            <div class="row">              
            	<div class="col l3 m3 s12">
                	<h5>Company</h5>                
	                <ul class="left-list">
	                  <li><a class="btn-float" href="#!">About Us</a></li>
	                  <li><a class="btn-float" href="#!">Contact</a></li>
	                  <li><a class="btn-float" href="#!">Blog</a></li>
	                  <li><a class="btn-float" href="#!">Advertise</a></li>
	                </ul>

	                <ul class="left-list">
	                  <li><a class="btn-float" href="#!">About Us</a></li>
	                  <li><a class="btn-float" href="#!">Contact</a></li>
	                  <li><a class="btn-float" href="#!">Blog</a></li>
	                  <li><a class="btn-float" href="#!">Advertise</a></li>
	                </ul>
                </div>
             
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                  <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>Join the Newsletter</h5>
                <ul>
                  <p>Get the latest listed property in your email.</p>
                  <input type="email" placeholder="example@example.com" id="email" class="validate"/>
                  <a class="waves-effect waves-light btn" href="#!">Subscribe</a>
                  
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                   <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="pattern">
          	<img src="../Cinqsnipelte/images/pattern.png">
          </div>

          <div class="copyrights">
          	<p class="reserve">2016 © Rentma®. All Rights reserved.</p>
          	<p class="search">Search & Find All Kinds of Rentals (Home, Apartment,Shop,Land & Rooms/Flat) For Rent.</p>
          	<a href="#!"><img src="../Cinqsnipelte/images/facebook.png"></a>
      		<a href="#!"><img src="../Cinqsnipelte/images/twitter.png"></a>
  			<a href="#!"><img src="../Cinqsnipelte/images/google+.png"></a>

          </div>
    </footer><!--footer ends-->



	<script type="text/javascript" src="../Cinqsnipelte/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="../Cinqsnipelte/js/materialize.min.js"></script>	
	<script type="text/javascript" src="../Cinqsnipelte/js/jquery.nicescroll.js"></script>
	<script type="text/javascript" src="../Cinqsnipelte/js/dragscrollable.js"></script>
	<script src="../Cinqsnipelte/js/jquery-ui.js"></script>
	<script type="text/javascript">

		  $(document).ready(function() {

            // $("carousel-demo1 .icons button,.main-box").click(function(a){
            //     a.preventDefault();
            // });

            // $('carousel-demo1 .icons button').tooltip({
            //     position: {
            //         my: "center bottom-20",
            //         at: "center top",
            //         using: function( position, feedback ) {
            //           $( this ).css( position );
            //           $( "<div>" )
            //             .addClass( "arrow" )
            //             .addClass( feedback.vertical )
            //             .addClass( feedback.horizontal )
            //             .appendTo( this );
            //         }
            //     },
            //     tooltipClass: "tooltip",
            //     effect: "fadeIn", 
            //     duration: 100
            // });


            $("content-here").niceScroll();
            $("mobile").niceScroll();

            $(".dropdown-button").dropdown({hover : true});

            $('signup').click(function(){
                $('.login-page').css('display','none');
                $('.signup-page').css('display','block');
            });

            $('.back').click(function(){
                $('.login-page').css('display','block');
                $('.signup-page').css('display','none');
            });

            $('.modal-trigger').leanModal({
                dismissible: false,             
            });




          $(".button-collapse").sideNav();  
          
         
            
            $('.close').click(function(e){
                e.preventDefault();
            });

            $('#modal-login').click(function(){
                $('#login-btn').css('display','none');
                $('.btn-collapse').css('display','block');
                $(this).css('display','none');
                $('.right').animate({
                    'padding-right': '150px'
                });
                $('.btn-collapse').sideNav({
                    menuWidth: 300, 
                    edge: 'right', 
                     
                });
            });

            $('#signup').click(function(){
                $('.login-page').css('display','none');
                $('.signup-page').css('display','block');
            });

            $('.back').click(function(){
                $('.login-page').css('display','block');
                $('.signup-page').css('display','none');
            });

            $('.modal-trigger').leanModal({
                dismissible: false,             
            });


           

            
            var loader = '<div class="preloader-wrapper active">\
                            <div class="spinner-layer spinner-blue-only">\
                              <div class="circle-clipper left">\
                                <div class="circle"></div>\
                              </div><div class="gap-patch">\
                                <div class="circle"></div>\
                              </div><div class="circle-clipper">\
                                <div class="circle"></div>\
                              </div>\
                            </div>\
                          </div>';


            $('.menu-list .menu-btn').click(function(e){
                e.preventDefault();
                var hide = $(this).data("hide");
                var scr = $( window ).width();  
                var height = $(window).height();
                var proHeaderHeight = $('.user-logo').height();
                var he = height-proHeaderHeight;            
                if(hide == true)
                {                   
                    if( scr > 600){             
                        $(this).parent().parent().animate({
                            width: '30%'
                        },800);
                        
                    }                   
                    else
                    {                       
                        $(this).parent().parent().animate({
                            width: '100%'
                        },800);

                    }
                    $('#content-here').css({
                        'height': height+'px'
                    });
                    $('#mobile li').css('display','none');
                    $('#mobile .close').css('display','block');
                    $('#mobile .user-logo').css('display','none');          

                }
                else
                {
                    if( scr > 600){
                        
                        $(this).parent().parent().animate({
                            width: '30%'
                        },800);
                    }                   
                    else
                    {
                        $(this).parent().parent().animate({
                            width: '100%'
                        },800);
                    }
                    
                    $('#content-here').css({
                        'height': '100%'
                    });
                    $('#mobile .user-logo').css({'position':'fixed','top':'0px'});
                    $('#mobile li').css('display','none');
                    $('#mobile .close').css('display','block');
                    $('#mobile .user-logo').css('display','block');
                    $('#mobile .user-logo #edit-btn').css('display','inline-block');
                }

                var url = $(this).attr('href');

                $.ajax({
                    url: url,
                    
                    beforeSend: function(data){
                        $('#content-here').html(loader);
                        
                    },
                    dataType: 'html',
                    success: function(data) {
                        setTimeout(function(){
                            $('#content-here').html(data);
                        },1000);
                        
                        
                    }
                });

                $('#mobile #content-here').css('display','block');
            });

            $('#mobile .close').click(function(){
                $(this).parent().animate({
                    width: 250
                },500);
                $('#mobile li').css('display','block');
                $(this).css('display','none');
                $('#mobile #content-here').css('display','none');
                $('#mobile .user-logo').css({'display':'block','position':'initial'});
                $('#mobile .user-logo #edit-btn').css('display','none');
                $('#content-here .profile .info-list #save-button').css('display','none');
            });

            $('#mobile .user-logo #edit-btn').click(function(){
                $('#content-here .profile .info-list input').removeAttr('readonly').eq(2).attr("type","text");
                $('#content-here .profile .info-list input').css('border-bottom','1px solid #9e9e9e');
                $('#content-here .profile .info-list #save-button').css('display','flex');
            });
        
            $('.icons, .belt').dragscrollable({
                dragSelector: '.belt', 
                acceptPropagatedEvent: true
            });

            $(window).resize(function(){
                card();             
            });
            card();
            

            function card(){
                var width = $(window).width();
                if(width <= 1024){
                    $("#carousel-demo1").mThumbnailScroller("destroy");
                    $('.carousel-demo, .carousel-wrap').dragscrollable({
                        dragSelector: '.carousel-wrap', 
                        acceptPropagatedEvent: true
                    });
                    //$('#carousel-demo1').css('padding-right','20px');
                }
                else{
                    (function($){
                        $(window).load(function(){
                            $("#carousel-demo1").mThumbnailScroller({
                              axis:"x", //change to "y" for vertical scroller
                              contentTouchScroll: false,
                              speed: 30
                            });
                        });
                    })(jQuery);
                }
            }

            $('#tenants-drop a').hover(function(){              
                $('.dropdown-tenants').slideToggle().toggleClass('active');
                $(this).find('.fa').toggleClass('rotate');
            });

            $('#landlord-drop a').hover(function(){                 
                $('.dropdown-landlord').slideToggle().toggleClass('active');
                $(this).find('.fa').toggleClass('rotate');
            });
                

        });



        window.onload = function () {
            var styles = [
                {
                    featureType: 'water',
                    elementType: 'geometry.fill',
                    stylers: [
                        { color: '#99b3ce' }
                    ]
                },{
                    featureType: 'landscape.natural',
                    elementType: 'all',
                    stylers: [
                        { color: '#eeeae1' },
                        
                        
                    ]
                },{
                     featureType: 'road',
                        elementType: 'geometry',
                        stylers: [
                            { color: '#ffffff' },
                           
                        ]
                },{
                     featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [
                            { color: '#fffb8d' },
                           
                        ]
                },{
                     featureType: 'transit.station.bus',
                        elementType: 'geometry',
                        stylers: [
                            { color: 'images/busstop.png' },
                           
                        ]
                }
            ];



               var options = {
            mapTypeControlOptions: {
            mapTypeIds: ['Styled']
            },
            center: new google.maps.LatLng(-7.245217594087794, 112.74455556869509),
            zoom: 16,
            disableDefaultUI: true, 
            mapTypeId: 'Styled'
            };
            var div = document.getElementById('map');
            var map = new google.maps.Map(div, options);
            var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
            map.mapTypes.set('Styled', styledMapType);      


        };


    </script>
</body>
</html>