@extends('blank')
@include('details.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('/details') }}">Home</a>
    </div>
    {{-- <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('/details/create') }}">New Post</a></li>
        <li><a href="{{ URL::to('') }}">Create a Nerd</a></li>
    </ul> --}}
</nav>

<h1>Post</h1>

<!-- if there are creation errors, they will show here -->
{{-- {{ HTML::ul($errors->all()) }}
 --}}
{{-- {{ Form::open(array('url' => 'nerds')) }} --}}
@foreach($errors->all() as $error)
                    <p class="alert alert-danger">Only Images is Allowed in image field</p>
                @endforeach
                @if(session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
   
<form class="form-horizontal" enctype="multipart/form-data" method="post" action="/details">
  {{csrf_field()}}
   {{--  <div>Add Post</div> --}}

  <div class="form-group">
    <label for="agent_name" class="col-sm-2 control-label">Agent Name</label>
    <div class="col-sm-8">
      <input required type="text" class="form-control" name="agent_name" placeholder="Name">
    </div>
  </div>

  <div class="form-group">
    <label for="agent_id" class="col-sm-2 control-label">Agent ID</label>
    <div class="col-sm-8">
      <input required type="text" class="form-control" name="agent_id" placeholder="Agent's ID">
    </div>
  </div>
      
  <div class="form-group">
    <label for="address" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-8">
      <input required type="text" class="form-control" name="address" placeholder="address">
    </div>
  </div>

 <div class="form-group">
    <label for="category" class="col-sm-2 control-label">Category</label>
    <div class="col-sm-8">
      <select name="category" class="form-control">
        <option>Home</option>
        <option>Apartment</option>
        <option>Land</option>
        <option>Flat|Room</option>
        <option>Shutter</option>
      </select>
    </div>
  </div>

  <div class="form-group">
    <label for="images" class="col-sm-2 control-label">Image</label>
    <div class="col-sm-8">
      <input required type="file" class="form-control" name="images[]" placeholder="address" multiple>
    </div>
  </div>

  <div class="form-group">
    <label for="price" class="col-sm-2 control-label">Price Rs.</label>
    <div class="col-sm-8">
      <input required type="int" class="form-control" name="price" placeholder="Price">
    </div>
  </div>

    <div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-8">
      <textarea rows="4" cols="50" name="description" placeholder="Description"></textarea>
    </div>
    </div>



     <div class="form-group">
    <label for="location" class="col-sm-2 control-label">Location</label>
    <div class="col-sm-8">
      <input required type="text" class="form-control" name="location" placeholder="location">
    </div>
  </div>

  
  <div class="form-group">
    <label for="no_of_rooms" class="col-sm-2 control-label">Total No. Of Rooms</label>
    <div class="col-sm-8">
      <input required type="integer" class="form-control" name="no_of_rooms" placeholder="Number of rooms..">
    </div>
  </div>


   <div class="form-group">
       <label for="owner_status" class="col-sm-2 control-label">Owner Lives There ?</label>
       <div class="col-sm-8">
       <select name="owner_status" class="form-control">
       <option>Home Owner Lives in Same building</option>
       <option>Home Owner doesnt Lives in the building</option>
       <option>For Sale</option>
       </select>
       </div>
       </div>


       <div class="form-group">
       <label for="isnegotiable" class="col-sm-2 control-label">Is Price Negotiable ?</label>
       <div class="col-sm-8">
       <select name="isnegotiable" class="form-control">
       <option>Yes</option>
       <option>No</option>
       </select>
       </div>
       </div>

  <div class="form-group">
    <label for="road_type" class="col-sm-2 control-label">Road Type</label>
    <div class="col-sm-8">
      <select name="road_type" class="form-control">
        <option>Main</option>
        <option>Market Road</option>
        <option>Highway</option>
      </select>
    </div>
  </div>


  

    <div class="col-sm-offset-2 col-sm-8">
      <input required type="submit" name="submit" class="btn btn-primary">
    </div>

</form>

</div>
@endsection