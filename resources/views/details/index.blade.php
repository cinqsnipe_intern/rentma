<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Rentma</title>
    <link rel="shortcut-icon" href="/Cinqsnipelte/build/favicon.io" >
    <link type="text/css" rel="stylesheet" href="Cinqsnipelte/css/materialize.min.css" >
    <link rel="stylesheet" type="text/css" href="Cinqsnipelte/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="Cinqsnipelte/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="Cinqsnipelte/css/animate.css">
    <link rel="stylesheet" type="text/css" href="Cinqsnipelte/css/hover.css">   
    <link rel="stylesheet" type="text/css" href="Cinqsnipelte/css/style.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <style type="text/css">
    
        
    </style>
</head>
<body>
    <!--[if lt IE 10]> 
        <p style="position: fixed; top: 10px; left: 20px; z-index: 999;">For better performance please open with upgraded version or update your browser.</p>
    <![endif]-->
    <div class="index-banner col l12 m12 s12">
        <span class="overlay"></span>

        <ul id="dropdown1" class="dropdown-content">
            <li><a href="{{route('fronts.list','Home')}}">Home</a></li>
                        <li><a href="{{route('fronts.list','Apartment')}}">Apartment</a></li>
                        <li><a href="{{route('fronts.list','Flat|Room')}}">Flat/Single Room</a></li>
                        <li><a href="{{route('fronts.list','Shutter')}} ">Shop/Shutter</a></li>
                        <li><a href="{{route('fronts.list','Land')}}">Land</a></li>
        </ul>

        <ul id="dropdown2" class="dropdown-content">
             <li><a href="{{route('fronts.list','Home')}}">Home</a></li>
                        <li><a href="{{route('fronts.list','Apartment')}}">Apartment</a></li>
                        <li><a href="{{route('fronts.list','Flat|Room')}}">Flat/Single Room</a></li>
                        <li><a href="{{route('fronts.list','Shutter')}} ">Shop/Shutter</a></li>
                        <li><a href="{{route('fronts.list','Land')}}">Land</a></li>
        </ul>       



        @include('partials._nav')
        <div class="container">
            <p>No matter where you are</p>
            <h4>Rent the <span>STAY</span> you need</h4>

            <a class="waves-effect waves-light search" data-status="on"><i class="fa fa-search"></i><span>Quick Search</span></a>                           
                    <div class="search-wrapper">
                        <div class="row">           
                        <div class="container">
                            <div class="property col l5 m12 s12">
                                <div class="input-field">
                                    <select>
                                      <option value="" disabled selected>Property Type</option>
                                      <option value="1">Home</option>
                                      <option value="2">Apartment</option>
                                      <option value="4">Flat/Room</option>
                                      <option value="5">Shop/Shutter</option>
                                      <option value="6">Land</option>
                                    </select>

                              </div>
                            </div>

                            <div class="search-input col l5 m12 s12">
                                <input type="text" placeholder="Location">
                            </div>

                            <div class="btn-wrapper col l2 m12 s12">                      
                                <a class="waves-effect waves-light btn search"><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                
                
          
             <div class="inner-images" >
                <img src="../Cinqsnipelte/build/banner.png" id="image" class="responsive-img">
            </div>  <!--inner-images ends-->
        </div>

        <div id="modal1" class="modal">
            <div class="modal-content">
                <div class="container">
                <div class="row">
                    <a data-status="on" class="modal-action modal-close"><i class="fa fa-times"></i></a>
                    <div class="logo">
                        <img src="Cinqsnipelte/images/logo1.png">
                    </div>
                    <div class="login-page">
                        <div class="acc">
                            <div class="account col l8 m8 s8">
                                <p>Don't have the account?</p>
                            </div>
                            <div class="signup col l4 m4 s4">
                                <a class="waves-effect waves-light btn" id="signup">Sign Up</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-field">
                            <form>
                                <input type="text" placeholder="Username / Email">
                                <input type="password" placeholder="Password">
                            </form>
                            <a class="waves-effect waves-light btn modal-action modal-close" id="modal-login">LOGIN</a>
                        </div>
                        <div class="or">
                            <div class="line">
                                <p>OR</p>
                            </div>                      
                        </div>
                        <div class="social-sites">
                            <a href=""><img src="Cinqsnipelte/images/facebook.png"></a>
                            <a href=""><img src="Cinqsnipelte/images/twitter.png"></a>
                            <a href=""><img src="Cinqsnipelte/images/google+.png"></a>
                        </div>
                    </div><!--login-page ends-->
                    <div class="signup-page">
                        <a class="back"><i class="fa fa-chevron-left"></i></a>
                        <form>
                            <input type="text" placeholder="Full Name">
                            <input type="text" placeholder="Username">
                            <input type="email" placeholder="abc@abc.com" class="validate">
                            <input type="password" placeholder="Password">
                            <input type="password" placeholder="Confirm Password">
                            <input type="number" placeholder="Phone Number">
                        </form>
                        <a class="waves-effect waves-light btn">Sign Up</a>
                    </div>                
                </div>
                </div>
            </div>
        </div><!--modal ends here-->


    </div><!--banner ends here-->

    <div class="category col l12 m12 s12">
        <div class="row">
        <div class="container-fluid">
            <div class="item-belt">
                <div class="item-main item-first">
                    <a href="{{route('fronts.list','Home')}} ">
                        <div class="item">
                            <img src="Cinqsnipelte/build/banner.png" alt="" />
                            <p>Home</p>
                        </div>
                    </a>
                </div>
                <div class="item-main">
                    <a href="{{route('fronts.list','Apartment')}} ">
                        <div class="item">
                            <img src="Cinqsnipelte/build/banner.png" alt="" />
                            <p>Apartment</p>
                        </div>
                    </a>
                </div>
                <div class="item-main">
                    <a href="{{route('fronts.list','Flat|Room')}} ">
                        <div class="item">
                            <img src="Cinqsnipelte/build/banner.png" alt="" />
                            <p>Flat/Single Room</p>
                        </div>
                    </a>
                </div>
                <div class="item-main">
                    <a href="{{route('fronts.list','Shop|Shutter')}} ">
                        <div class="item">
                            <img src="Cinqsnipelte/build/banner.png" alt="" />
                            <p>Shop/Shutter</p>
                        </div>
                    </a>
                </div>
                <div class="item-main item-last">
                    <a href="{{route('fronts.list','Land')}} ">
                        <div class="item">
                            <img src="Cinqsnipelte/build/banner.png" alt="" />
                            <p>Land</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        </div>
    </div><!--category ends-->

    <div class="features">
        <p class="not">Not all free, Paid too</p>
        <h4>Featured <span>Listings</span></h4>   

                <div id="carousel-demo1" class="carousel-demo">
            <div class="belt-card">
                <ul class="carousel-wrap">
                @foreach($data as $a)
                    <li class="carousel-wrap-two">                  
                        <div class="all">

                        
                        <a href="{{route('details.show',$a->id)}} " class="main-box">

                                 @if(isset(explode('|', $a->images)[0]))
                                  <div class="display-image">
                                    <img src="/image/{{explode('|', $a->images)[0]}}"/><br/>
                                        <span class="caption">{{$a->location}}/ {{$a->category}} </span>
                                  </div>
                                @endif
                                 
                                <div class="card-info">                             
                                    <p class="cost">Rs.{{$a->price}}/mo. </p>
                                    <p class="need">{{substr($a->description, 0, 30)}}{{strlen($a->description) > 30 ? "..." : "" }} </p>
                                    <div class="icons icon1" >
                                        <div class="belt">
                                        
                                        <button title="Air-conditioned" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/ac.svg">
                                            
                                        </button>   
                                         <button title="Security Camera" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/cc.svg">
                                            
                                         </button>  
                                         <button title="Electricity" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/electricity.svg">
                                            
                                         </button>  
                                         <button title="Elevator" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/elevator.svg">
                                            
                                         </button>  
                                         <button title="Furnished" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/furnished.svg">
                                            
                                         </button>  
                                         <button title="Garden" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/garden.svg">
                                            
                                         </button>
                                         <button title="Gym" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/gym.svg">
                                            
                                         </button>  
                                         <button title="Internet" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/internet.svg">
                                            
                                         </button>  
                                         <button title="East Faced" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/east.svg">
                                            
                                         </button>  
                                         <button title="West Faced" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/west.svg">
                                            
                                         </button>  
                                         <button title="North Faced" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/north.svg">
                                            
                                         </button>  
                                         <button title="South Faced" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/south.svg">
                                            
                                         </button>  
                                         <button title="Parking" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/parking.svg">
                                            
                                         </button>  
                                         <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/pets.svg">
                                            
                                         </button>  
                                         <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/pool.svg">
                                            
                                         </button>  
                                         <button title="Shopping Center" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/shopping.svg">
                                            
                                         </button>  
                                         <button title="Solar" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/solar.svg">
                                            
                                         </button>  
                                         <button title="Theatre" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/theatre.svg">
                                            
                                         </button>  
                                         <button title="Water 24*7" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/water.svg">
                                            
                                         </button>  
                                         <button title="Power Backup" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/power.svg">
                                            
                                         </button>  
                                         <button title="Television" class="btn-floating waves-effect waves-light">
                                            <img src="Cinqsnipelte/feature-icons/tv.svg">
                                            
                                         </button>  
                                        
                                        </div>
                                    </div>

                                    <p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
                                    <p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
                                    <div class="clearfix"></div>                                
                                </div>
                            </a>
                            <a href="#" class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>   
                             @endforeach        
                        </div>                      
                    </li>
                   

                    
                    


                </ul>
            </div><!--end of belt-card-->
        </div> <!--carousel-demo1 ends-->
    

        <div class="work">
            <p class="use">Easy to use</p>
            <h4>HOW <span>It Works</span></h4>
            <div class="row">
                <div class="container-fluid">              
                    <div id="test1" class="test">
                        <div class="fishing col l3 m3">
                            <img src="Cinqsnipelte/images/fishing1.svg">
                            <h5>List Your vacant property</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.  </p>
                        </div>
                        <div class="arrow col l1 m1">
                            <img src="Cinqsnipelte/images/arrow1.png">
                        </div>
                        <div class="fishing col l3 m3">
                            <img src="Cinqsnipelte/images/fishing2.svg">
                            <h5>grab the potential tenant</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. </p>
                        </div>
                        <div class="arrow col l1 m1">
                            <img src="Cinqsnipelte/images/arrow2.png">
                        </div>
                        <div class="fishing col l3 m3">
                            <img src="Cinqsnipelte/images/fishtrapped.svg">
                            <h5>deal and stay happy</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. </p>
                        </div>
                    </div><!--test1 ends-->             
                </div>
            </div><!--row ends-->

            {{-- <ul class="bxslider">
                <li>
                    <div class="custom-slider">
                    <img src="Cinqsnipelte/images/fishing1.png" />
                    <h5>deal and stay happy</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </li>
                 <li>
                    <div class="custom-slider">
                    <img src="Cinqsnipelte/images/fishing2.png" />
                    <h5>deal and stay happy</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </li>
                <li>
                <div class="custom-slider">
                <img src="Cinqsnipelte/images/fishtrapped.png" />
                <h5>deal and stay happy</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. </p>
                </div>
                 </li>
                 
            </ul>
 --}}
        </div><!--work ends-->

    </div><!--Cinqsnipelte/feature ends-->

    <div class="review col l12 m12 s12">
        <p>Love and trusted</p>
        <h4>WOW <span>Reviews</span></h4>

                    
                <div class="row">
                    <div class="custom-card col l12 ">
                    <div class="col l6 m6 s12">
                        <div class="content">
                            <div class="img">
                                <img src="Cinqsnipelte/images/card.png" class="circle">
                            </div>
                            <div class="desc">
                                <h5>Agelia Junie Lobets, UK</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat.</p>  
                            </div>
                        </div>
                    </div>
                    
                    <div class="col l6 m6 s12">
                        <div class="content">
                            <div class="img">
                                <img src="Cinqsnipelte/images/card.png" class="circle">
                            </div>
                            <div class="desc">
                                <h5>Agelia Junie Lobets, UK</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat.</p>  
                            </div>
                        </div>
                    </div>
                    </div>
                </div>          
    </div><!--review ends-->

    <div class="total">
        <div class="row">
            <div class="container">
                <div class="list col m3 l3 s12">
                    <img src="Cinqsnipelte/build/banner.png" alt="" />
                    <h4>1052</h4>
                    <p>Property Listed</p>
                </div>
                <div class="list col m3 l3 s12">
                    <img src="Cinqsnipelte/build/banner.png" alt="" />
                    <h4>3956</h4>
                    <p>User Registered</p>
                </div>
                <div class="list col m3 l3 s12">
                    <img src="Cinqsnipelte/build/banner.png" alt="" />
                    <h4>568</h4>
                    <p>Property Rented</p>
                </div>
                <div class="list col m3 l3 s12">
                    <img src="Cinqsnipelte/build/banner.png" alt="" />
                    <h4>1052</h4>
                    <p>Project Listed</p>
                </div>          
            </div>
        </div>
    </div><!--total ends-->


    <footer class="page-footer">
          <div class="container">
            <div class="row">
              
              <div class="col l3 m3 s12">
                <h5>Company</h5>
                
                    <ul class="left-list">
                      <li><a class="btn-float" href="#!">About Us</a></li>
                      <li><a class="btn-float" href="#!">Contact</a></li>
                      <li><a class="btn-float" href="#!">Blog</a></li>
                      <li><a class="btn-float" href="#!">Advertise</a></li>
                    </ul>
                

               
                    <ul class="left-list">
                      <li><a class="btn-float" href="#!">About Us</a></li>
                      <li><a class="btn-float" href="#!">Contact</a></li>
                      <li><a class="btn-float" href="#!">Blog</a></li>
                      <li><a class="btn-float" href="#!">Advertise</a></li>
                    </ul>
                </div>
             
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                  <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>Join the Newsletter</h5>
                <ul>
                  <p>Get the latest listed property in your email.</p>
                  <input type="email" placeholder="example@example.com" id="email" class="validate"/>
                  <a class="waves-effect waves-light btn" href="#!">Subscribe</a>
                  
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                   <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="pattern">
            <img src="Cinqsnipelte/images/pattern.png">
          </div>

          <div class="copyrights">
            <p class="reserve">2016 © Rentma®. All Rights reserved.</p>
            <p class="search">Search & Find All Kinds of Rentals (Home, Apartment,Shop,Land & Rooms/Flat) For Rent.</p>
            <a href="#!"><img src="Cinqsnipelte/images/facebook.png"></a>
                <a href="#!"><img src="Cinqsnipelte/images/twitter.png"></a>
                    <a href="#!"><img src="Cinqsnipelte/images/google+.png"></a>

          </div>
    </footer><!--footer ends-->





    <script type="text/javascript" src="/Cinqsnipelte/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/Cinqsnipelte/js/materialize.min.js"></script>    
    <script type="text/javascript" src="/Cinqsnipelte/js/jquery.nicescroll.js"></script>
    <script type="text/javascript" src="/Cinqsnipelte/js/dragscrollable.js"></script>
    <script src="/Cinqsnipelte/js/jquery-ui.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {

            // $("carousel-demo1 .icons button,.main-box").click(function(a){
            //     a.preventDefault();
            // });

            // $('carousel-demo1 .icons button').tooltip({
            //     position: {
            //         my: "center bottom-20",
            //         at: "center top",
            //         using: function( position, feedback ) {
            //           $( this ).css( position );
            //           $( "<div>" )
            //             .addClass( "arrow" )
            //             .addClass( feedback.vertical )
            //             .addClass( feedback.horizontal )
            //             .appendTo( this );
            //         }
            //     },
            //     tooltipClass: "tooltip",
            //     effect: "fadeIn", 
            //     duration: 100
            // });


            // $("#content-here").niceScroll();
            // $("#mobile").niceScroll();

            $(".dropdown-button").dropdown({hover : true});

            $('signup').click(function(){
                $('.login-page').css('display','none');
                $('.signup-page').css('display','block');
            });

            $('.back').click(function(){
                $('.login-page').css('display','block');
                $('.signup-page').css('display','none');
            });

            $('.modal-trigger').leanModal({
                dismissible: false,             
            });




          $(".button-collapse").sideNav();  
          
         
            
            $('.close').click(function(e){
                e.preventDefault();
            });

            $('#modal-login').click(function(){
                $('#login-btn').css('display','none');
                $('.btn-collapse').css('display','block');
                $(this).css('display','none');
                $('.right').animate({
                    'padding-right': '150px'
                });
                $('.btn-collapse').sideNav({
                    menuWidth: 300, 
                    edge: 'right', 
                     
                });
            });

            $('#signup').click(function(){
                $('.login-page').css('display','none');
                $('.signup-page').css('display','block');
            });

            $('.back').click(function(){
                $('.login-page').css('display','block');
                $('.signup-page').css('display','none');
            });

            $('.modal-trigger').leanModal({
                dismissible: false,             
            });


           

            
            var loader = '<div class="preloader-wrapper active">\
                            <div class="spinner-layer spinner-blue-only">\
                              <div class="circle-clipper left">\
                                <div class="circle"></div>\
                              </div><div class="gap-patch">\
                                <div class="circle"></div>\
                              </div><div class="circle-clipper">\
                                <div class="circle"></div>\
                              </div>\
                            </div>\
                          </div>';


            $('.menu-list .menu-btn').click(function(e){
                e.preventDefault();
                var hide = $(this).data("hide");
                var scr = $( window ).width();  
                var height = $(window).height();
                var proHeaderHeight = $('.user-logo').height();
                var he = height-proHeaderHeight;            
                if(hide == true)
                {                   
                    if( scr > 600){             
                        $(this).parent().parent().animate({
                            width: '30%'
                        },800);
                        
                    }                   
                    else
                    {                       
                        $(this).parent().parent().animate({
                            width: '100%'
                        },800);

                    }
                    $('#content-here').css({
                        'height': height+'px'
                    });
                    $('#mobile li').css('display','none');
                    $('#mobile .close').css('display','block');
                    $('#mobile .user-logo').css('display','none');          

                }
                else
                {
                    if( scr > 600){
                        
                        $(this).parent().parent().animate({
                            width: '30%'
                        },800);
                    }                   
                    else
                    {
                        $(this).parent().parent().animate({
                            width: '100%'
                        },800);
                    }
                    
                    $('#content-here').css({
                        'height': '100%'
                    });
                    $('#mobile .user-logo').css({'position':'fixed','top':'0px'});
                    $('#mobile li').css('display','none');
                    $('#mobile .close').css('display','block');
                    $('#mobile .user-logo').css('display','block');
                    $('#mobile .user-logo #edit-btn').css('display','inline-block');
                }

                var url = $(this).attr('href');

                $.ajax({
                    url: url,
                    
                    beforeSend: function(data){
                        $('#content-here').html(loader);
                        
                    },
                    dataType: 'html',
                    success: function(data) {
                        setTimeout(function(){
                            $('#content-here').html(data);
                        },1000);
                        
                        
                    }
                });

                $('#mobile #content-here').css('display','block');
            });

            $('#mobile .close').click(function(){
                $(this).parent().animate({
                    width: 250
                },500);
                $('#mobile li').css('display','block');
                $(this).css('display','none');
                $('#mobile #content-here').css('display','none');
                $('#mobile .user-logo').css({'display':'block','position':'initial'});
                $('#mobile .user-logo #edit-btn').css('display','none');
                $('#content-here .profile .info-list #save-button').css('display','none');
            });

            $('#mobile .user-logo #edit-btn').click(function(){
                $('#content-here .profile .info-list input').removeAttr('readonly').eq(2).attr("type","text");
                $('#content-here .profile .info-list input').css('border-bottom','1px solid #9e9e9e');
                $('#content-here .profile .info-list #save-button').css('display','flex');
            });
        
            $('.icons, .belt').dragscrollable({
                dragSelector: '.belt', 
                acceptPropagatedEvent: true
            });

            $(window).resize(function(){
                card();             
            });
            card();
            

            function card(){
                var width = $(window).width();
                if(width <= 1024){
                    $("#carousel-demo1").mThumbnailScroller("destroy");
                    $('.carousel-demo, .carousel-wrap').dragscrollable({
                        dragSelector: '.carousel-wrap', 
                        acceptPropagatedEvent: true
                    });
                    //$('#carousel-demo1').css('padding-right','20px');
                }
                else{
                    (function($){
                        $(window).load(function(){
                            $("#carousel-demo1").mThumbnailScroller({
                              axis:"x", //change to "y" for vertical scroller
                              contentTouchScroll: false,
                              speed: 30
                            });
                        });
                    })(jQuery);
                }
            }

            $('#tenants-drop a').hover(function(){              
                $('.dropdown-tenants').slideToggle().toggleClass('active');
                $(this).find('.fa').toggleClass('rotate');
            });

            $('#landlord-drop a').hover(function(){                 
                $('.dropdown-landlord').slideToggle().toggleClass('active');
                $(this).find('.fa').toggleClass('rotate');
            });
                

        });



        window.onload = function () {
            var styles = [
                {
                    featureType: 'water',
                    elementType: 'geometry.fill',
                    stylers: [
                        { color: '#99b3ce' }
                    ]
                },{
                    featureType: 'landscape.natural',
                    elementType: 'all',
                    stylers: [
                        { color: '#eeeae1' },
                        
                        
                    ]
                },{
                     featureType: 'road',
                        elementType: 'geometry',
                        stylers: [
                            { color: '#ffffff' },
                           
                        ]
                },{
                     featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [
                            { color: '#fffb8d' },
                           
                        ]
                },{
                     featureType: 'transit.station.bus',
                        elementType: 'geometry',
                        stylers: [
                            { color: 'images/busstop.png' },
                           
                        ]
                }
            ];



               var options = {
            mapTypeControlOptions: {
            mapTypeIds: ['Styled']
            },
            center: new google.maps.LatLng(-7.245217594087794, 112.74455556869509),
            zoom: 16,
            disableDefaultUI: true, 
            mapTypeId: 'Styled'
            };
            var div = document.getElementById('map');
            var map = new google.maps.Map(div, options);
            var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
            map.mapTypes.set('Styled', styledMapType);      


        };


    </script>
    
</body>
</html>