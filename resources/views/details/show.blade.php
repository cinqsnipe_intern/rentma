<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>rentma details</title>
    
    <link type="text/css" rel="stylesheet" href="../Cinqsnipelte/css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/font-awesome.css"> 
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/wow-style.css">
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/hover.css">
    <link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
</head>
<body>
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="#!">Home</a></li>
            <li><a href="#!">Apartment</a></li>         
            <li><a href="#!">Flat/Single Room</a></li>
            <li><a href="#!">Shop/Shutter</a></li>
            <li><a href="#!">Land</a></li>
        </ul>

        <ul id="dropdown2" class="dropdown-content">
            <li><a href="#!">Home</a></li>
            <li><a href="#!">Apartment</a></li>         
            <li><a href="#!">Flat/Single Room</a></li>
            <li><a href="#!">Shop/Shutter</a></li>
            <li><a href="#!">Land</a></li>
        </ul>       



            @include('partials._nav')

        <div id="modal1" class="modal">
            <div class="modal-content">
                <div class="container">
                <div class="row">
                    <a data-status="on" class="modal-action modal-close"><i class="fa fa-times"></i></a>
                    <div class="logo">
                        <img src="../Cinqsnipelte/images/logo1.png">
                    </div>
                    <div class="login-page">
                        <div class="acc">
                            <div class="account col l8 m8 s8">
                                <p>Don't have the account?</p>
                            </div>
                            <div class="signup col l4 m4 s4">
                                <a class="waves-effect waves-light btn" id="signup">Sign Up</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-field">
                            <form>
                                <input type="text" placeholder="Username / Email">
                                <input type="password" placeholder="Password">
                            </form>
                            <a class="waves-effect waves-light btn modal-action modal-close" id="modal-login">LOGIN</a>
                        </div>
                        <div class="or">
                            <div class="line">
                                <p>OR</p>
                            </div>                      
                        </div>
                        <div class="social-sites">
                            <a href=""><img src="../Cinqsnipelte/images/facebook.png"></a>
                            <a href=""><img src="../Cinqsnipelte/images/twitter.png"></a>
                            <a href=""><img src="../Cinqsnipelte/images/google+.png"></a>
                        </div>
                    </div><!--login-page ends-->
                    <div class="signup-page">
                        <a class="back"><i class="fa fa-chevron-left"></i></a>
                        <form>
                            <input type="text" placeholder="Full Name">
                            <input type="text" placeholder="Username">
                            <input type="email" placeholder="abc@abc.com" class="validate">
                            <input type="password" placeholder="Password">
                            <input type="password" placeholder="Confirm Password">
                            <input type="number" placeholder="Phone Number">
                        </form>
                        <a class="waves-effect waves-light btn">Sign Up</a>
                    </div>                
                </div>
                </div>
            </div>
        </div><!--modal ends here-->

<div class="details-content">
        <div class="banner"></div>  

        <div class="details">
            <div class="row">
                <div class="container">

                <nav>
                    <h4>Subidha Yukta Ghar Bhadama</h4>
                    <div class="nav-wrapper">
                      <div class="col s12 bread">
                        <a href="{{route('fronts.list','Home')}} " class="breadcrumb">Home</a>
                        <a href="{{route('fronts.list','Apartment')}} " class="breadcrumb">Residential Home</a>
                        <a href="{{route('fronts.list','Shutter')}}" class="breadcrumb">Shutter</a>
                      </div>
                    </div>
                </nav>


            
<div class="details-section-wrapper col l9 m12 s12">
                    <div class="details-section col l12 m12 s12">       

                        <div class="interiors col l12 m12 s12">
                            <div class="int-images">                 
                                <!-- Start WOWSlider.com BODY section -->
                            <div id="wowslider-container1">
                            <div class="ws_images">
                                <ul>
                                    @foreach(explode('|',$data->images) as $image)
                                           <li><img src="/image/{{$image}}" alt="" title="" id=""></li>
                                         @endforeach
                                    
                                    
                            </div>
                            
                            <div class="ws_thumbs">
                                <div>
                                    @foreach(explode('|',$data->images) as $image)
                                    <a href="#" title="a">
                                           <li><img src="/image/{{$image}}" alt="" title="" id=""></li>
                                          alt="" /></a>
                                @endforeach
                                 </div>
                               </div>
                            </div>

                            </div><!--int-images ends-->

                            
                            <div class="clearfix"></div>

                            <div class="info col l12 m12 s12">
                                
                                <div class="price1 col l8 m8 s12">
                                    <p class="address"><i class="fa fa-map-marker"></i>{{$data->address}} </p>
                                </div>
                                <div class="price2 col l4 m4 s12">
                                    <p class="rate">{{$data->price}} /mo</p>
                                </div>
                                
                                <div class="propic">
                                    <a href="{{route('agents.show',$agentdetail[0]->id)}}"><img src="../agentspic/{{$agentdetail[0]->images}}" class="circle">
                                

                                    <p>By <span><a href="{{route('agents.show',$data->agent_id)}}">{{$data->agent_name}}</a></span></p>

                                </div>
                                <div class="clearfix"></div>
                                <div class="line"></div>                            


                            </div><!--info ends-->

                        </div><!--interior ends-->

                        <div class="clearfix"></div>

                        <div class="main-features col l12">
                            <h6>Main Features</h6>
                            <div class="icons">
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/ac.svg">
                                    <span>Air Conditioned</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/cc.svg">
                                    <span>Security Camera</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/electricity.svg">
                                    <span>Electricity</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/elevator.svg">
                                    <span>Elevator</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/furnished.svg">
                                    <span>Furnished</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/garden.svg">
                                    <span>Garden</span>
                                 </button>
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/gym.svg">
                                    <span>Fitness Center</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/internet.svg">
                                    <span>No Internet Facility</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/east.svg">
                                    <span>{{$agentdetail[0]->name}}</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/west.svg">
                                    <span></span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/north.svg">
                                    <span>North Faced</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/south.svg">
                                    <span>South Faced</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/parking.svg">
                                    <span>Parking Lot</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/pets.svg">
                                    <span>Pets Allowed</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/pool.svg">
                                    <span>Swimming Pool</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/shopping.svg">
                                    <span>Shopping Center</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/solar.svg">
                                    <span>Solar</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/theatre.svg">
                                    <span>Theatre</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/water.svg">
                                    <span>Water</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/power.svg">
                                    <span>Power Backup</span>
                                 </button>  
                                 <button class="btn-floating waves-effect waves-light">
                                    <img src="../Cinqsnipelte/feature-icons/tv.svg">
                                    <span>Television</span>
                                 </button>      
                            </div>
                        </div><!--features ends-->

                        <div class="additional col l12">
                            <div class="add col l6 m6 s12">
                                <p>Additional Info</p>
                            </div>
                            <div class="home col l6 m6 s12">
                                <p><img src="../Cinqsnipelte/images/sign.png"> {{$data->owner_status}}.</p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="information col l4 m12 s12">
                                <p>Total no. of rooms</p>
                                <p class="bold">{{$data->no_of_rooms}}</p>
                            </div>
                            <div class="information col l4 m12 s12">
                                <p>Type of road</p>
                                <p class="bold">{{$data->road_type}}</p>
                            </div>
                            <div class="information col l4 m12 s12">
                                <p>Is price negotiable?</p>
                                <p class="bold">{{$data->isnegotiable}}</p>
                            </div>

                        </div><!--additional ends-->

                        <div class="desc col l12">
                            <h6>Description</h6>
                            <p>Description
                                {{$data->description}}
                            </p>
                        </div><!--desc ends-->

                        <div class="amneties col l12 m12 s12" >
                            <h6>View Amneties</h6>
                             <div id="map" style="width:100%;height:380px;"></div>
                        </div><!--amneties ends-->

                        <div class="clearfix"></div>


                        <div class="btns">
                            <a class="waves-effect waves-light btn fav">Add to fav</a>
                            <a class="waves-effect waves-light btn meet">Rent It</a>
                        </div>  
                            
                        
                                       
                    </div><!--details-section ends-->
                </div>

                    <div class="variety col l3 m12 s12">
                        
                            <div class="adv  col l12 m6 s12">
                                <img src="../Cinqsnipelte/images/turbine.png">
                            </div><!--adv ends-->                   

                            <div class="similar col l12 m6 s12">
                                <div class="top">
                                    <h5>Similar Property You May Love</h5>
                                        <div class="propic">
                                            <img src="../Cinqsnipelte/images/propic.png" class="circle">
                                            <p>By <span>Suman Jung</span></p>
                                        </div>
                                </div>
                                <div class="image">
                                    <img src="../Cinqsnipelte/images/similar.png">
                                </div>
                                <div class="bottom">
                                    <h5>{{$data->description}}}</h5>
                                    <h4>NRP 15000</h4>
                                    <p><i class="fa fa-map-marker"></i> Koila Galli, Wotu, New road, Kathmandu</p>
                                </div>
                            </div><!--similar ends-->
                    
                        
                    </div><!--variety ends-->
                </div>
            </div>
        </div><!--details ends-->

                
            @include('partials.belt')
        

                    <li class="als-item">
                        <div class="all2">
                            <img class="logo" src="../Cinqsnipelte/build/banner.png"/>              
                            <p class="rent">Now get the <br> property for RENT or <br> give the property for RENT. </p> 
                            <a href="#" class="waves-effect waves-light btn">I'm landlord</a>
                            <a href="#" class="waves-effect waves-light btn">I'm tenant</a>         
                        </div>
                    </li>

                    


                </ul>
            </div><!--end of belt-card-->
        </div> <!--carousel-demo1 ends-->
        </div><!--details-features ends-->


        <div class="details-total">
            <div class="row">
                <div class="container">
                    <div class="list col m3 l3 s12">
                        <img src="../Cinqsnipelte/build/banner.png" alt="" />
                        <h4>1052</h4>
                        <p>Property Listed</p>
                    </div>
                    <div class="list col m3 l3 s12">
                        <img src="../Cinqsnipelte/build/banner.png" alt="" />
                        <h4>3956</h4>
                        <p>User Registered</p>
                    </div>
                    <div class="list col m3 l3 s12">
                        <img src="../Cinqsnipelte/build/banner.png" alt="" />
                        <h4>568</h4>
                        <p>Property Rented</p>
                    </div>
                    <div class="list col m3 l3 s12">
                        <img src="../Cinqsnipelte/build/banner.png" alt="" />
                        <h4>1052</h4>
                        <p>Project Listed</p>
                    </div>          
                </div>
            </div>
        </div><!--details-total ends-->
</div><!--details-content ends-->


         <footer class="page-footer">
          <div class="container">
            <div class="row">
              
              <div class="col l3 m3 s12">
                <h5>Company</h5>
                
                    <ul class="left-list">
                      <li><a class="btn-float" href="#!">About Us</a></li>
                      <li><a class="btn-float" href="#!">Contact</a></li>
                      <li><a class="btn-float" href="#!">Blog</a></li>
                      <li><a class="btn-float" href="#!">Advertise</a></li>
                    </ul>
                

               
                    <ul class="left-list">
                      <li><a class="btn-float" href="#!">About Us</a></li>
                      <li><a class="btn-float" href="#!">Contact</a></li>
                      <li><a class="btn-float" href="#!">Blog</a></li>
                      <li><a class="btn-float" href="#!">Advertise</a></li>
                    </ul>
                </div>
             
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                  <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>Join the Newsletter</h5>
                <ul>
                  <p>Get the latest listed property in your email.</p>
                  <input type="email" placeholder="example@example.com" id="email" class="validate"/>
                  <a class="waves-effect waves-light btn" href="#!">Subscribe</a>
                  
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                   <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="pattern">
            <img src="../Cinqsnipelte/images/pattern.png">
          </div>

          <div class="copyrights">
            <p class="reserve">2016 © Rentma®. All Rights reserved.</p>
            <p class="search">Search & Find All Kinds of Rentals (Home, Apartment,Shop,Land & Rooms/Flat) For Rent.</p>
            <a href="#!"><img src="../Cinqsnipelte/images/facebook.png"></a>
                <a href="#!"><img src="../Cinqsnipelte/images/twitter.png"></a>
                    <a href="#!"><img src="../Cinqsnipelte/images/google+.png"></a>

          </div>
        </footer><!--footer ends-->






    <script type="text/javascript" src="../Cinqsnipelte/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../Cinqsnipelte/js/jquery.mThumbnailScroller.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>    
    <script type="text/javascript" src="../Cinqsnipelte/js/wowslider.js"></script>
    <script type="text/javascript" src="../Cinqsnipelte/js/script.js"></script>
    <script type="text/javascript" src="../Cinqsnipelte/js/materialize.min.js"></script>
    <script type="text/javascript" src="../Cinqsnipelte/js/dragscrollable.js"></script>
    <script type="text/javascript" src="../Cinqsnipelte/js/jquery.nicescroll.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            // $("carousel-demo1 .icons button,.main-box").click(function(a){
            //     a.preventDefault();
            // });

            // $('carousel-demo1 .icons button').tooltip({
            //     position: {
            //         my: "center bottom-20",
            //         at: "center top",
            //         using: function( position, feedback ) {
            //           $( this ).css( position );
            //           $( "<div>" )
            //             .addClass( "arrow" )
            //             .addClass( feedback.vertical )
            //             .addClass( feedback.horizontal )
            //             .appendTo( this );
            //         }
            //     },
            //     tooltipClass: "tooltip",
            //     effect: "fadeIn", 
            //     duration: 100
            // });


            $("content-here").niceScroll();
            $("mobile").niceScroll();

            $(".dropdown-button").dropdown({hover : true});

            $('signup').click(function(){
                $('.login-page').css('display','none');
                $('.signup-page').css('display','block');
            });

            $('.back').click(function(){
                $('.login-page').css('display','block');
                $('.signup-page').css('display','none');
            });

            $('.modal-trigger').leanModal({
                dismissible: false,             
            });




          $(".button-collapse").sideNav();  
          
         
            
            $('.close').click(function(e){
                e.preventDefault();
            });

            $('#modal-login').click(function(){
                $('#login-btn').css('display','none');
                $('.btn-collapse').css('display','block');
                $(this).css('display','none');
                $('.right').animate({
                    'padding-right': '150px'
                });
                $('.btn-collapse').sideNav({
                    menuWidth: 300, 
                    edge: 'right', 
                     
                });
            });

            $('#signup').click(function(){
                $('.login-page').css('display','none');
                $('.signup-page').css('display','block');
            });

            $('.back').click(function(){
                $('.login-page').css('display','block');
                $('.signup-page').css('display','none');
            });

            $('.modal-trigger').leanModal({
                dismissible: false,             
            });


           

            
            var loader = '<div class="preloader-wrapper active">\
                            <div class="spinner-layer spinner-blue-only">\
                              <div class="circle-clipper left">\
                                <div class="circle"></div>\
                              </div><div class="gap-patch">\
                                <div class="circle"></div>\
                              </div><div class="circle-clipper">\
                                <div class="circle"></div>\
                              </div>\
                            </div>\
                          </div>';


            $('.menu-list .menu-btn').click(function(e){
                e.preventDefault();
                var hide = $(this).data("hide");
                var scr = $( window ).width();  
                var height = $(window).height();
                var proHeaderHeight = $('.user-logo').height();
                var he = height-proHeaderHeight;            
                if(hide == true)
                {                   
                    if( scr > 600){             
                        $(this).parent().parent().animate({
                            width: '30%'
                        },800);
                        
                    }                   
                    else
                    {                       
                        $(this).parent().parent().animate({
                            width: '100%'
                        },800);

                    }
                    $('#content-here').css({
                        'height': height+'px'
                    });
                    $('#mobile li').css('display','none');
                    $('#mobile .close').css('display','block');
                    $('#mobile .user-logo').css('display','none');          

                }
                else
                {
                    if( scr > 600){
                        
                        $(this).parent().parent().animate({
                            width: '30%'
                        },800);
                    }                   
                    else
                    {
                        $(this).parent().parent().animate({
                            width: '100%'
                        },800);
                    }
                    
                    $('#content-here').css({
                        'height': '100%'
                    });
                    $('#mobile .user-logo').css({'position':'fixed','top':'0px'});
                    $('#mobile li').css('display','none');
                    $('#mobile .close').css('display','block');
                    $('#mobile .user-logo').css('display','block');
                    $('#mobile .user-logo #edit-btn').css('display','inline-block');
                }

                var url = $(this).attr('href');

                $.ajax({
                    url: url,
                    
                    beforeSend: function(data){
                        $('#content-here').html(loader);
                        
                    },
                    dataType: 'html',
                    success: function(data) {
                        setTimeout(function(){
                            $('#content-here').html(data);
                        },1000);
                        
                        
                    }
                });

                $('#mobile #content-here').css('display','block');
            });

            $('#mobile .close').click(function(){
                $(this).parent().animate({
                    width: 250
                },500);
                $('#mobile li').css('display','block');
                $(this).css('display','none');
                $('#mobile #content-here').css('display','none');
                $('#mobile .user-logo').css({'display':'block','position':'initial'});
                $('#mobile .user-logo #edit-btn').css('display','none');
                $('#content-here .profile .info-list #save-button').css('display','none');
            });

            $('#mobile .user-logo #edit-btn').click(function(){
                $('#content-here .profile .info-list input').removeAttr('readonly').eq(2).attr("type","text");
                $('#content-here .profile .info-list input').css('border-bottom','1px solid #9e9e9e');
                $('#content-here .profile .info-list #save-button').css('display','flex');
            });
        
            $('.icons, .belt').dragscrollable({
                dragSelector: '.belt', 
                acceptPropagatedEvent: true
            });

            $(window).resize(function(){
                card();             
            });
            card();
            

            function card(){
                var width = $(window).width();
                if(width <= 1024){
                    $("#carousel-demo1").mThumbnailScroller("destroy");
                    $('.carousel-demo, .carousel-wrap').dragscrollable({
                        dragSelector: '.carousel-wrap', 
                        acceptPropagatedEvent: true
                    });
                    //$('#carousel-demo1').css('padding-right','20px');
                }
                else{
                    (function($){
                        $(window).load(function(){
                            $("#carousel-demo1").mThumbnailScroller({
                              axis:"x", //change to "y" for vertical scroller
                              contentTouchScroll: false,
                              speed: 30
                            });
                        });
                    })(jQuery);
                }
            }

            $('#tenants-drop a').hover(function(){              
                $('.dropdown-tenants').slideToggle().toggleClass('active');
                $(this).find('.fa').toggleClass('rotate');
            });

            $('#landlord-drop a').hover(function(){                 
                $('.dropdown-landlord').slideToggle().toggleClass('active');
                $(this).find('.fa').toggleClass('rotate');
            });
                

        });



        window.onload = function () {
            var styles = [
                {
                    featureType: 'water',
                    elementType: 'geometry.fill',
                    stylers: [
                        { color: '#99b3ce' }
                    ]
                },{
                    featureType: 'landscape.natural',
                    elementType: 'all',
                    stylers: [
                        { color: '#eeeae1' },
                        
                        
                    ]
                },{
                     featureType: 'road',
                        elementType: 'geometry',
                        stylers: [
                            { color: '#ffffff' },
                           
                        ]
                },{
                     featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [
                            { color: '#fffb8d' },
                           
                        ]
                },{
                     featureType: 'transit.station.bus',
                        elementType: 'geometry',
                        stylers: [
                            { color: 'images/busstop.png' },
                           
                        ]
                }
            ];



               var options = {
            mapTypeControlOptions: {
            mapTypeIds: ['Styled']
            },
            center: new google.maps.LatLng(-7.245217594087794, 112.74455556869509),
            zoom: 16,
            disableDefaultUI: true, 
            mapTypeId: 'Styled'
            };
            var div = document.getElementById('map');
            var map = new google.maps.Map(div, options);
            var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
            map.mapTypes.set('Styled', styledMapType);      


        };


    </script>
    
</body>
</html>