<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>	
	<script type="text/javascript" src="js/jquery.nicescroll.js"></script>
	<script type="text/javascript" src="js/dragscrollable.js"></script>
	<script type="text/javascript">

		$(document).ready(function() {
			$("#content-here").niceScroll();

	    	$("#mobile").niceScroll();

	    	
		    $('select').material_select();
		    

			$(".dropdown-button").dropdown({hover : true});

			$(".button-collapse").sideNav();

			

			$('.close').click(function(e){
				e.preventDefault();
			});

				
			$('#modal-login').click(function(){
				$('#login-btn').css('display','none');
				$('.btn-collapse').css('display','block');
				$(this).css('display','none');
				$('.right').animate({
					'padding-right': '150px'
				});
				$('.btn-collapse').sideNav({
			      	menuWidth: 300, 
			      	edge: 'right', 
			      	 
		    	});
			});

			$('#signup').click(function(){
				$('.login-page').css('display','none');
				$('.signup-page').css('display','block');
			});

			$('.back').click(function(){
				$('.login-page').css('display','block');
				$('.signup-page').css('display','none');
			});

			$('.modal-trigger').leanModal({
				dismissible: false,				
			});

	    
		    var loader = '<div class="preloader-wrapper active">\
						    <div class="spinner-layer spinner-blue-only">\
						      <div class="circle-clipper left">\
						        <div class="circle"></div>\
						      </div><div class="gap-patch">\
						        <div class="circle"></div>\
						      </div><div class="circle-clipper">\
						        <div class="circle"></div>\
						      </div>\
						    </div>\
						  </div>';


		    $('.menu-list .menu-btn').click(function(e){
		    	e.preventDefault();
		    	var hide = $(this).data("hide");
		    	var scr = $( window ).width();	
		    	var height = $(window).height();
		     	var proHeaderHeight = $('.user-logo').height();
		     	var he = height-proHeaderHeight;	    	
		    	if(hide == true)
		    	{		    		
		    		if( scr > 600){    			
		    		   	$(this).parent().parent().animate({
				      		width: '30%'
				      	},800);
				      	
			     	}			     	
			     	else
			     	{			     		
			     		$(this).parent().parent().animate({
				      		width: '100%'
				      	},800);

			     	}
			     	$('#content-here').css({
			     		'height': height+'px'
			     	});
			      	$('#mobile li').css('display','none');
			      	$('#mobile .close').css('display','block');
			      	$('#mobile .user-logo').css('display','none');	      	

		      	}
		      	else
		      	{
			      	if( scr > 600){
		    			
				     	$(this).parent().parent().animate({
				      		width: '30%'
				      	},800);
			     	}			     	
			     	else
			     	{
			     		$(this).parent().parent().animate({
				      		width: '100%'
				      	},800);
			     	}
			     	
			     	$('#content-here').css({
			     		'height': '100%'
			     	});
					$('#mobile .user-logo').css({'position':'fixed','top':'0px'});
			      	$('#mobile li').css('display','none');
			      	$('#mobile .close').css('display','block');
			      	$('#mobile .user-logo').css('display','block');
			      	$('#mobile .user-logo #edit-btn').css('display','inline-block');
		      	}

		    	var url = $(this).attr('href');

		      	$.ajax({
				    url: url,
				    
				    beforeSend: function(data){
				    	$('#content-here').html(loader);
				    	
				    },
				    dataType: 'html',
				    success: function(data) {
				    	setTimeout(function(){
				    		$('#content-here').html(data);
				    	},1000);
				        
				        
				    }
				});

				$('#mobile #content-here').css('display','block');
		    });

		    $('#mobile .close').click(function(){
		    	$(this).parent().animate({
		      		width: 250
		      	},500);
		      	$('#mobile li').css('display','block');
		      	$(this).css('display','none');
		      	$('#mobile #content-here').css('display','none');
		      	$('#mobile .user-logo').css({'display':'block','position':'initial'});
		      	$('#mobile .user-logo #edit-btn').css('display','none');
		      	$('#content-here .profile .info-list #save-button').css('display','none');
		    });

		    $('#mobile .user-logo #edit-btn').click(function(){
		    	$('#content-here .profile .info-list input').removeAttr('readonly').eq(2).attr("type","text");
		    	$('#content-here .profile .info-list input').css('border-bottom','1px solid #9e9e9e');
		    	$('#content-here .profile .info-list #save-button').css('display','flex');
		    });

		    $('#tenants-drop a').click(function(){ 				
				$('.dropdown-tenants').slideToggle().toggleClass('active');
				$(this).find('.fa').toggleClass('rotate');
			});

			$('#landlord-drop a').click(function(){ 				
				$('.dropdown-landlord').slideToggle().toggleClass('active');
				$(this).find('.fa').toggleClass('rotate');
			});

			$('.scrolling-belt, .scrolling-div').dragscrollable({
			    dragSelector: '.scrolling-div', 
			    acceptPropagatedEvent: true
			});

			$(window).resize(function(){
					container();
				});

			container();

			function container(){
				var listWidth = $(window).width();
				if(listWidth > 992 && listWidth < 1050){
					$('#agent-container').removeClass('container').addClass('container-fluid');
				}
			}


		});