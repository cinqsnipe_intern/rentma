<nav>
            <div class="container-fluid">
                <div class="nav-wrapper">
                  <a href="/details" class="brand-logo"><img src="../Cinqsnipelte/images/logo1.png"></a>
                  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="fa fa-bars"></i></a>         

                  <ul class="right right-nav ">

                    <div class="hide-on-med-and-down">

                        <li><a href="#"  class="dropdown-button" data-activates="dropdown2">For Tenants <span><i class="fa fa-chevron-down"></i></span></a></li>
                        <li><a href="#"  class="dropdown-button" data-activates="dropdown1">For Landlord <span><i class="fa fa-chevron-down"></i></span></a></li>
                        <li><a href="{{route('agents.index')}} ">Agents</a></li>                    
                        <a href="{{route('details.create')}}" class="waves-effect waves-light btn">
                        Post Now</a>
                    </div>                   
                    <a href="{{ url('/login') }}" class="waves-effect waves-light btn login modal-trigger" data-target="modal1" id="login-btn">Login/Sign Up</a>
                  </ul>

                  <ul class="side-nav menu" id="mobile-demo">
                    <li id="tenants-drop"><a>For Tenants <span><i class="fa fa-chevron-down"></i></span></a></li>
                    <div class="dropdown-tenants active">
                         <li><a href="/list#home">Home</a></li>
                        <li><a href="/list#apartment">Apartment</a></li>
                        <li><a href="/list#flat">Flat/Single Room</a></li>
                        <li><a href="/list#shop">Shop/Shutter</a></li>
                        <li><a href="/list#land">Land</a></li>
                    </div>
                    <li id="landlord-drop"><a>For Landlord <span><i class="fa fa-chevron-down"></i></span></a></li>
                    <div class="dropdown-landlord active">
                        <li><a href="/list#home">Home</a></li>
                        <li><a href="/list#apartment">Apartment</a></li>
                        <li><a href="/list#flat">Flat/Single Room</a></li>
                        <li><a href="/list#shop">Shop/Shutter</a></li>
                        <li><a href="/list#land">Land</a></li>
                    </div>
                    <li><a href="Cinqsnipelte/agents.html">Agents</a></li>  
                    <a href="Cinqsnipelte/post.html" class="waves-effect waves-light btn">Post Now</a>
                   
                  </ul>

                   <div class="user">
                        <a href="#" data-activates="mobile" class="btn-collapse"><img src="Cinqsnipelte/images/card.png" class="circle"><span class="hvr-underline-from-center">Suman Jung</span></a>
                        <ul class="side-nav " id="mobile">
                            <div class="user-logo">                             
                                <img src="Cinqsnipelte/images/card.png" class="circle">
                                <div>
                                    <p>Suman Jung</p>
                                    <a class="btn" id="edit-btn">Edit <i class="fa fa-pencil"></i></a>
                                </div>                      
                            </div>
                            <div class="clearfix"></div>
                            <li class="menu-list">
                                <a class="menu-btn" href="Cinqsnipelte/notification.html" data-hide="true">
                                    <img src="Cinqsnipelte/images/notification-icon.png">
                                    <span class="notifier"></span>
                                    <span class="name">Notifications</span>
                                    <span class="arrow"><i class="fa fa-chevron-right"></i></span>
                                </a>
                            </li>
                            <li class="menu-list">
                                <a class="menu-btn" href="Cinqsnipelte/profile.html" data-hide="false">
                                    <img src="Cinqsnipelte/images/profile-icon.png">
                                    <span class="name">My Profile</span>
                                    <span class="arrow"><i class="fa fa-chevron-right"></i></span>
                                </a>
                            </li>
                            <li class="menu-list">
                                <a class="menu-btn" href=Cinqsnipelte/mypost.html" data-hide="true">
                                    <img src="Cinqsnipelte/images/post-icon.png">
                                    <span class="name">My Posts</span>
                                    <span class="arrow"><i class="fa fa-chevron-right"></i></span>
                                </a>
                            </li>
                            <li class="menu-list">
                                <a class="menu-btn" href="Cinqsnipelte/message.html" data-hide="true">
                                    <img src=Cinqsnipelte/images/message-icon.png">
                                    <span class="notifier"></span>
                                    <span class="name">Messages</span>
                                    <span class="arrow"><i class="fa fa-chevron-right"></i></span>
                                </a>
                            </li>
                             <li class="menu-list">
                                <a class="menu-btn" href="Cinqsnipelte/favourites.html" data-hide="true">
                                    <img src="Cinqsnipelte/images/favourite-icon.png">
                                    <span class="name">Favourites</span>
                                    <span class="arrow"><i class="fa fa-chevron-right"></i></span>
                                </a>
                            </li>
                             <li class="menu-list">
                                <a href="#!" data-hide="none">
                                    <img src="Cinqsnipelte/images/logout-icon.png">
                                    <span class="name">Logout</span>                        
                                </a>
                            </li>
                            <button class="close"><i class="fa fa-chevron-left"></i></button>
                            <div id="content-here"></div>
                        </ul>
                    </div><!--user ends-->

                </div>
            </div>
        </nav>

