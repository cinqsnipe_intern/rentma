                  

                    @foreach($lists as $a)
                          <div class="card-one col l4 m6 s12">
											<div class="all">
												<a href="{{route('details.show',$a->id)}}" class="main-box">
													
                                 @if(isset(explode('|', $a->images)[0]))
                                  <div class="display-image">
                                    <img src="/image/{{explode('|', $a->images)[0]}}"/><br/>
                                    <span class="caption">{{$a->location}}</span>
                                  </div>
                                @endif
											      	<div class="card-info">					      	
												      	<p class="cost">{{$a->price}} </p>
												      	<p class="need">{{substr($a->description, 0, 30)}}{{strlen($a->description) > 30 ? "..." : "" }}</p>
												      	<div class="icons icon1" >
												      		<div class="belt">
															<button title="Air-conditioned" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/ac.svg">
							    							 	
							    							</button>	
							    							 <button title="Security Camera" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/cc.svg">
							    							 	
							    							 </button>	
							    							 <button title="Electricity" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/electricity.svg">
							    							 	
							    							 </button>	
							    							 <button title="Elevator" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/elevator.svg">
							    							 	
							    							 </button>	
							    							 <button title="Furnished" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/furnished.svg">
							    							 	
							    							 </button>	
							    							 <button title="Garden" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/garden.svg">
							    							 	
							    							 </button>
							    							 <button title="Gym" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/gym.svg">
							    							 	
							    							 </button>	
							    							 <button title="Internet" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/internet.svg">
							    							 	
							    							 </button>	
							    							 <button title="East Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/east.svg">
							    							 	
							    							 </button>	
							    							 <button title="West Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/west.svg">
							    							 	
							    							 </button>	
							    							 <button title="North Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/north.svg">
							    							 	
							    							 </button>	
							    							 <button title="South Faced" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/south.svg">
							    							 	
							    							 </button>	
							    							 <button title="Parking" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/parking.svg">
							    							 	
							    							 </button>	
							    							 <button title="Pets Allowed" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pets.svg">
							    							 	
							    							 </button>	
							    							 <button title="Swimming Pool" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/pool.svg">
							    							 	
							    							 </button>	
							    							 <button title="Shopping Center" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/shopping.svg">
							    							 	
							    							 </button>	
							    							 <button title="Solar" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/solar.svg">
							    							 	
							    							 </button>	
							    							 <button title="Theatre" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/theatre.svg">
							    							 	
							    							 </button>	
							    							 <button title="Water 24*7" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/water.svg">
							    							 	
							    							 </button>	
							    							 <button title="Power Backup" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/power.svg">
							    							 	
							    							 </button>	
							    							 <button title="Television" class="btn-floating waves-effect waves-light">
							    							 	<img src="../Cinqsnipelte/feature-icons/tv.svg">
							    							 	
							    							 </button>	
															
															</div>
														</div>

												      	<p class="fav"><i class="fa fa-heart"> <span>78 favs</span></i></p>
												      	<p class="view"><i class="fa fa-eye"> <span>1667 views</span></i></p>
												      	<div class="clearfix"></div>						      	
											      	</div>
											    </a>
										      	<a class="btn-floating btn-large waves-effect waves-light fav-btn"><i class="fa fa-heart-o"></i></a>			
									        </div>	
									      </div>
									        @endforeach 
									 
									 
