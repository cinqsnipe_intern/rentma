<!DOCTYPE html>
<html>
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<title>rentma list</title>
	<link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/nouislider.css">
	<link type="text/css" rel="stylesheet" href="../Cinqsnipelte/css/materialize.min.css"  media="screen,projection"/>
	<link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/jquery.range.css">
	<link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/hover.css">	
	<link rel="stylesheet" type="text/css" href="../Cinqsnipelte/css/style.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
</head>
<body>

	<ul id="dropdown1" class="dropdown-content">
			 <li><a href="fronts.list#home">Home</a></li>
                        <li><a href="/list#apartment">Apartment</a></li>
                        <li><a href="/list#flat">Flat/Single Room</a></li>
                        <li><a href="/list#shop">Shop/Shutter</a></li>
                        <li><a href="/list#land">Land</a></li>
		</ul>

		<ul id="dropdown2" class="dropdown-content">
			 <li><a href="/list#home">Home</a></li>
                        <li><a href="/list#apartment">Apartment</a></li>
                        <li><a href="/list#flat">Flat/Single Room</a></li>
                        <li><a href="/list#shop">Shop/Shutter</a></li>
                        <li><a href="/list#land">Land</a></li>
		</ul>		



		<nav>
			<div class="container-fluid">
			    <div class="nav-wrapper">
			      <a href="/" class="brand-logo"><img src="../Cinqsnipelte/images/logo1.png"></a>
			      <a href="/" data-activates="mobile-demo" class="button-collapse"><i class="fa fa-bars"></i></a>	      

			      <ul class="right right-nav ">
			      	<div class="hide-on-med-and-down">
				        <li><a href="/details"  class="dropdown-button" data-activates="dropdown2">For Tenants <span><i class="fa fa-chevron-down"></i></span></a></li>
				        <li><a href="/list"  class="dropdown-button" data-activates="dropdown1">For Landlord <span><i class="fa fa-chevron-down"></i></span></a></li>
				        <li><a href="/agents">Agents</a></li>		        	      
				        {{-- <a href="/" class="waves-effect waves-light btn">Post Now</a> --}}
			        
			        <a class="waves-effect waves-light btn login modal-trigger" data-target="modal1" id="login-btn">Login</a>
			      </div>
			      </ul>

			      <ul class="side-nav menu" id="mobile-demo">
			        <li id="tenants-drop"><a>For Tenants <span><i class="fa fa-chevron-down"></i></span></a></li>
			        <div class="dropdown-tenants active">
			        	 <li><a href="/list#home">Home</a></li>
                        <li><a href="/list#apartment">Apartment</a></li>
                        <li><a href="/list#flat">Flat/Single Room</a></li>
                        <li><a href="/list#shop">Shop/Shutter</a></li>
                        <li><a href="/list#land">Land</a></li>
			        </div>
			        <li id="landlord-drop"><a>For Landlord <span><i class="fa fa-chevron-down"></i></span></a></li>
			        <div class="dropdown-landlord active">
			        	 <li><a href="/list#home">Home</a></li>
                        <li><a href="/list#apartment">Apartment</a></li>
                        <li><a href="/list#flat">Flat/Single Room</a></li>
                        <li><a href="/list#shop">Shop/Shutter</a></li>
                        <li><a href="/list#land">Land</a></li>
			        </div>
			        <li><a href="/agents">Agents</a></li>		
			        <a href="post.html" class="waves-effect waves-light btn">Post Now</a>
			       
			      </ul>

			       <div class="user">
				      	<a href="#" data-activates="mobile" class="btn-collapse"><img src="images/card.png" class="circle"><span class="hvr-underline-from-center">Suman Jung</span></a>
				      	<ul class="side-nav " id="mobile">
				      		<div class="user-logo">				      			
				      			<img src="images/card.png" class="circle">
				      			<div>
				      				<p>Suman Jung</p>
				      				<a class="btn" id="edit-btn">Edit <i class="fa fa-pencil"></i></a>
				      			</div>		      			
				      		</div>
				      		<div class="clearfix"></div>
					        <li class="menu-list">
					        	<a class="menu-btn" href="notification.html" data-hide="true">
					        		<img src="images/notification-icon.png">
					        		<span class="notifier"></span>
					        		<span class="name">Notifications</span>
					        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
					        	</a>
					        </li>
					        <li class="menu-list">
					        	<a class="menu-btn" href="profile.html" data-hide="false">
					        		<img src="images/profile-icon.png">
					        		<span class="name">My Profile</span>
					        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
					        	</a>
					        </li>
					        <li class="menu-list">
					        	<a class="menu-btn" href="mypost.html" data-hide="true">
					        		<img src="images/post-icon.png">
					        		<span class="name">My Posts</span>
					        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
					        	</a>
					        </li>
					        <li class="menu-list">
					        	<a class="menu-btn" href="message.html" data-hide="true">
					        		<img src="images/message-icon.png">
					        		<span class="notifier"></span>
					        		<span class="name">Messages</span>
					        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
					        	</a>
					        </li>
					         <li class="menu-list">
					        	<a class="menu-btn" href="favourites.html" data-hide="true">
					        		<img src="images/favourite-icon.png">
					        		<span class="name">Favourites</span>
					        		<span class="arrow"><i class="fa fa-chevron-right"></i></span>
					        	</a>
					        </li>
					         <li class="menu-list">
					        	<a href="#!">
					        		<img src="images/logout-icon.png">
					        		<span class="name">Logout</span>
					        		
					        	</a>
					        </li>
					    	<button class="close"><i class="fa fa-chevron-left"></i></button>
					    	<div id="content-here"></div>
				      	</ul>
				    </div><!--user ends-->

			    </div>
			</div>
    	</nav>


    	<div id="modal1" class="modal">
		    <div class="modal-content">
			    <div class="container">
			    <div class="row">
			    	<a data-status="on" class="modal-action modal-close"><i class="fa fa-times"></i></a>
			    	<div class="logo">
			    		<img src="../Cinqsnipelte/images/logo1.png">
			    	</div>

			    	<div class="login-page">
				      	<div class="acc">
				      		<div class="account col l8 m8 s8">
				      			<p>Don't have the account?</p>
				      		</div>
				      		<div class="signup col l4 m4 s4">
				      			<a class="waves-effect waves-light btn" id="signup">Sign Up</a>
				      		</div>
				      	</div>
				      	<div class="clearfix"></div>
				      	<div class="form-field">
				      		<form>
				      			<input type="text" placeholder="Username / Email">
				      			<input type="password" placeholder="Password">
				      		</form>
				      		<a class="waves-effect waves-light btn modal-action modal-close" id="modal-login">LOGIN</a>
				      	</div>
				      	<div class="or">
				      		<div class="line">
				      			<p>OR</p>
				      		</div>			      		
				      	</div>
				      	<div class="social-sites">
				      		<a href=""><img src="../Cinqsnipelte/images/facebook.png"></a>
				      		<a href=""><img src="../Cinqsnipelte/images/twitter.png"></a>
				      		<a href=""><img src="../Cinqsnipelte/images/google+.png"></a>
				      	</div>
			      	</div><!--login-page ends-->
			      	<div class="signup-page">
			      		<a class="back"><i class="fa fa-chevron-left"></i></a>
			      		<form>
			      			<input type="text" placeholder="Full Name">
			      			<input type="text" placeholder="Username">
			      			<input type="email" placeholder="abc@abc.com" class="validate">
			      			<input type="password" placeholder="Password">
			      			<input type="password" placeholder="Confirm Password">
			      			<input type="number" placeholder="Phone Number">
			      		</form>
			      		<a class="waves-effect waves-light btn">Sign Up</a>
			      	</div>			      
	    		</div>
	    		</div>
    		</div>
    	</div><!--modal ends here-->


    
		<div class="list-content">
				   <div class="row">
				    <div class="menu col s12">
				      <ul class="tabs">
				        <li class="tab"><a href="{{route('fronts.list')}}">Home</a></li>
				        <li class="tab" class="active"><a  href="#apartment">Apartment</a></li>
				        <li class="tab"><a href="#flat" >Flat/Single Room</a></li>
				        <li class="tab"><a href="#shop" >Shop/Shutter</a></li>
				         <li class="tab"><a href="#land">Land</a></li>
				      </ul>
				    </div>		   
				  </div>        
		    

	    	<div class="details">
	    		<div class="row">
	    			<div class="container">
	    				<div class="details-section col l9 m12 s12">   


	    					<div id="home">

	    						@include('partials.homelist')
	    					</div>
	    					<div id="apartment">
	    						@include('partials.apartmentlist')
	    					</div>
	    					

							<div class="preloader-wrapper big active">
								    <div class="spinner-layer spinner-blue-only">
								      <div class="circle-clipper left">
								        <div class="circle"></div>
								      </div><div class="gap-patch">
								        <div class="circle"></div>
								      </div><div class="circle-clipper">
								        <div class="circle"></div>
								      </div>
								    </div>
							</div>	 		
						
	    				</div><!--details-section ends-->

	    				<div class="variety col l3 m12 s12"> 				

	    						<div id="filter-wrapper" class="col l12 m12 s12">				
	    							<form class="filter-form" action="#">					    

	    							<div class="dropdown-filter">
									    <div class="home-filter filter-active">
									    	<div class="input-field">
											    <select>
											      <option value="" disabled selected>Property Type</option>
											      <option value="1">Commercial</option>
											      <option value="2">Residential</option>      
											    </select>								    
											</div>
										</div><!--home-filter ends-->

										<div class="apartment-filter">
									    	<div class="input-field">
											    <select>
											      <option value="" disabled selected>Property Type</option>
											      <option value="1">Commercial</option>
											      <option value="2">Residential</option>      
											    </select>								    
											</div>
										</div><!--apartment-filter ends-->

										<div class="flat-filter"></div>
										<div class="shop-filter"></div>

										<div class="land-filter">
											<div class="input-field">
											    <select>
											      <option value="" disabled selected>Property Type</option>
											      <option value="1">On Sale</option>
											      <option value="2">On Lease</option>      
											    </select>								    
											</div>
										</div><!--land-filter ends-->

									</div>	

										<input id="location" type="text" placeholder="Search By Location"/>


										<p class="range-field" style="margin-top: 0;">
										    <input type="hidden" data-min="" data-max="" id="range-value">
										    <label for="range">Price Range</label>
										    <div id="range"></div>
								    	</p>

								    	<div class="clearfix"></div>

								    <div class="additional-filter">	
									    <div class="home-filter filter-active">
									    	<label for="room-check">No. of Rooms</label>
									    	<div class="room-wrap" id="room-check">
										    	<p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio1" />
											      <label for="radio1">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio2" />
											      <label for="radio2">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio3" />
											      <label for="radio3">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio4" />
											      <label for="radio4">4-8</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio5" />
											      <label for="radio5">9-16</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio6" />
											      <label for="radio6">Any</label>
											    </p>    
										    </div>

										    <div class="clearfix"></div>

										    <label for="stories-check">No. of Stories</label>
									    	<div class="room-wrap" id="stories-check">
										    	<p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio7" />
											      <label for="radio7">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio8" />
											      <label for="radio8">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio9" />
											      <label for="radio9">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio10" />
											      <label for="radio10">4</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio11" />
											      <label for="radio11">5</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio12" />
											      <label for="radio12">Any</label>
											    </p>    
										    </div>
									    </div><!--home-filter ends-->						

										<div class="apartment-filter">
									    	<label for="room-check">No. of Rooms</label>
									    	<div class="room-wrap" id="room-check">
										    	<p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio13" />
											      <label for="radio13">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio14" />
											      <label for="radio14">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio15" />
											      <label for="radio15">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio16" />
											      <label for="radio16">4-8</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio17" />
											      <label for="radio17">9-16</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio18" />
											      <label for="radio18">Any</label>
											    </p>    
										    </div>

										    <div class="clearfix"></div>

										    <label for="stories-check">Preferred Floor</label>
									    	<div class="room-wrap" id="stories-check">
										    	<p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio19" />
											      <label for="radio19">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio820" />
											      <label for="radio820">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio21" />
											      <label for="radio21">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio22" />
											      <label for="radio22">4</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio23" />
											      <label for="radio23">5</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio24" />
											      <label for="radio24">Any</label>
											    </p>    
										    </div>
									    </div><!--apartment-filter ends-->

									    <div class="flat-filter">
									    	<label for="room-check">No. of Rooms</label>
									    	<div class="room-wrap" id="room-check">
										    	<p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio25" />
											      <label for="radio25">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio26" />
											      <label for="radio26">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio27" />
											      <label for="radio27">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio28" />
											      <label for="radio28">4-8</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio29" />
											      <label for="radio29">9-16</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group2" type="radio" id="radio30" />
											      <label for="radio30">Any</label>
											    </p>    
										    </div>

										    <div class="clearfix"></div>

										    <label for="stories-check">Preferred Floor</label>
									    	<div class="room-wrap" id="stories-check">
										    	<p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio31" />
											      <label for="radio31">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio32" />
											      <label for="radio32">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio33" />
											      <label for="radio33">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio34" />
											      <label for="radio34">4</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio35" />
											      <label for="radio35">5</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio36" />
											      <label for="radio36">Any</label>
											    </p>    
										    </div>
									    </div><!--flat-filter ends-->

									    <div class="shop-filter">
									    	<label for="room-check">No. of Shutters</label>
									    	<div class="room-wrap" id="room-check">
										    	<p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio37" />
											      <label for="radio37">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio38" />
											      <label for="radio38">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio39" />
											      <label for="radio39">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio40" />
											      <label for="radio40">4-8</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio41" />
											      <label for="radio41">9-16</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio42" />
											      <label for="radio42">Any</label>
											    </p>    
										    </div>

										    <div class="clearfix"></div>										    
									    </div><!--shop-filter ends-->


									    <div class="land-filter">
									    	<div class="input-field">
											    <select>
											      <option value="" disabled selected>Unit</option>		     					      
											      <option value="1"> aana</option>
											      <option value="2"> kattha</option>
											      <option value="3"> biga</option>
											      <option value="4"> ropani</option>         
											    </select>								    
											</div>

									    	<label for="room-check">Land Area</label>
									    	<div class="room-wrap" id="room-check">
										    	<p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio43" />
											      <label for="radio43">1</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio44" />
											      <label for="radio44">2</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio45" />
											      <label for="radio45">3</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio46" />
											      <label for="radio46">4-8</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio417" />
											      <label for="radio417">9-16</label>
											    </p>
											    <p class="radio-p col s2">
											      <input name="group1" type="radio" id="radio48" />
											      <label for="radio48">Any</label>
											    </p>    
										    </div>

										    <div class="clearfix"></div>										    
									    </div><!--land-filter ends-->



									</div><!--additional-filter ends-->		

									    
									    <button type="submit" class="waves-effect waves-light btn filter-search">Search</button>



									</form>


	    						 							
	    						</div><!--filter ends-->
		    				
		    					<div class="adv col l12 m6 s12">
		    						<img src="../Cinqsnipelte/images/turbine.png">
		    					</div><!--adv ends-->   				

		    					<div class="similar col l12 m6 s12">
		    						<div class="top">
		    							<h5>Sponsored Property</h5>    							
		    							<div class="propic">
		    								<img src="../Cinqsnipelte/images/propic.png" class="circle">
		    								<p>By <span>Suman Jung</span></p>
		    							</div>
		    						</div>
		    						<div class="image">
		    							<img src="../Cinqsnipelte/images/similar.png">
		    						</div>
		    						<div class="bottom">
		    							<h5>Subhida Yukta Ghar Bhadama</h5>
		    							<h4>NRP 15000</h4>
		    							<p><i class="fa fa-map-marker"></i> Koila Galli, Wotu, New road, Kathmandu</p>
		    						</div>
		    						<a class="waves-effect waves-light btn sponsor">Sponsor</a>
		    					</div><!--similar ends-->  	

	    					</div><!--variety ends-->
	    			</div><!--container ends-->
	    		</div><!--row ends-->
	    	</div><!--details ends-->    	

			<div class="list-total">
				<div class="row">
					<div class="container">
						<div class="list col m3 l3 s12">
							<img src="../Cinqsnipelte/build/banner.png" alt="" />
							<h4>1052</h4>
							<p>Property Listed</p>
						</div>
						<div class="list col m3 l3 s12">
							<img src="../Cinqsnipelte/build/banner.png" alt="" />
							<h4>3956</h4>
							<p>User Registered</p>
						</div>
						<div class="list col m3 l3 s12">
							<img src="../Cinqsnipelte/build/banner.png" alt="" />
							<h4>568</h4>
							<p>Property Rented</p>
						</div>
						<div class="list col m3 l3 s12">
							<img src="../Cinqsnipelte/build/banner.png" alt="" />
							<h4>1052</h4>
							<p>Project Listed</p>
						</div>			
					</div>
				</div>
			</div><!--total ends-->
		</div><!--list-content ends-->


		 <footer class="page-footer">
          <div class="container">
            <div class="row">
              
              <div class="col l3 m3 s12">
                <h5>Company</h5>
                
	                <ul class="left-list">
	                  <li><a class="btn-float" href="#!">About Us</a></li>
	                  <li><a class="btn-float" href="#!">Contact</a></li>
	                  <li><a class="btn-float" href="#!">Blog</a></li>
	                  <li><a class="btn-float" href="#!">Advertise</a></li>
	                </ul>
                

               
	                <ul class="left-list">
	                  <li><a class="btn-float" href="#!">About Us</a></li>
	                  <li><a class="btn-float" href="#!">Contact</a></li>
	                  <li><a class="btn-float" href="#!">Blog</a></li>
	                  <li><a class="btn-float" href="#!">Advertise</a></li>
	                </ul>
                </div>
             
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                  <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>Join the Newsletter</h5>
                <ul>
                  <p>Get the latest listed property in your email.</p>
                  <input type="email" placeholder="example@example.com" id="email" class="validate"/>
                  <a class="waves-effect waves-light btn" href="#!">Subscribe</a>
                  
                </ul>
              </div>
               <div class="col l3 m3 s12">
                <h5>International Sites</h5>
                <ul>
                   <li><a class="btn-float" href="#!">Sweden</a></li>
                  <li><a class="btn-float" href="#!">USA</a></li>
                  <li><a class="btn-float" href="#!">Israel</a></li>
                  <li><a class="btn-float" href="#!">Singapore</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="pattern">
          	<img src="../Cinqsnipelte/images/pattern.png">
          </div>

          <div class="copyrights">
          	<p class="reserve">2016 © Rentma®. All Rights reserved.</p>
          	<p class="search">Search & Find All Kinds of Rentals (Home, Apartment,Shop,Land & Rooms/Flat) For Rent.</p>
          	<a href="#!"><img src="../Cinqsnipelte/images/facebook.png"></a>
          		<a href="#!"><img src="../Cinqsnipelte/images/twitter.png"></a>
          			<a href="#!"><img src="../Cinqsnipelte/images/google+.png"></a>

          </div>
        </footer><!--footer ends-->


	<script type="text/javascript" src="../Cinqsnipelte/js/jquery-1.11.3.min.js"></script>
	
	<script type="text/javascript" src="../Cinqsnipelte/js/nouislider.js"></script>	
	<script type="text/javascript" src="../Cinqsnipelte/js/materialize.min.js"></script>
	<script src="../Cinqsnipelte/js/jquery-ui.js"></script>
	<script type="text/javascript" src="../Cinqsnipelte/js/jquery.range.js"></script>
	<script type="text/javascript" src="../Cinqsnipelte/js/jquery.nicescroll.js"></script>
	
	
	
	<script type="text/javascript">
		$(document).ready(function(){


				// $(".card-one .icons button,.card-one .main-box").click(function(a){
				// 	a.preventDefault();
				// });

				// $(document).tooltip({
				// 	position: {
				//         my: "center bottom-20",
				//         at: "center top",
				//         using: function( position, feedback ) {
				//           $( this ).css( position );
				//           $( "<div>" )
				//             .addClass( "arrow" )
				//             .addClass( feedback.vertical )
				//             .addClass( feedback.horizontal )
				//             .appendTo( this );
				//         }
				//     },
				//     tooltipClass: "tooltip",
				//     effect: "fadeIn", 
				//     duration: 100
				// });

				$("content-here").niceScroll();

		    	$("mobile").niceScroll();

		    	$(document).ready(function() {
			    	$('select').material_select();
			    });

				$(".dropdown-button").dropdown({hover : true});

				$(".button-collapse").sideNav();

				

	    		$('.close').click(function(e){
					e.preventDefault();
				});

					
				// $('#modal-login').click(function(){
				// 	$('#login-btn').css('display','none');
				// 	$('.btn-collapse').css('display','block');
				// 	$(this).css('display','none');
				// 	$('.right').animate({
				// 		'padding-right': '150px'
				// 	});
				// 	$('.btn-collapse').sideNav({
				//       	menuWidth: 300, 
				//       	edge: 'right', 
				      	 
			 //    	});
				// });

				// $('#signup').click(function(){
				// 	$('.login-page').css('display','none');
				// 	$('.signup-page').css('display','block');
				// });

				// $('.back').click(function(){
				// 	$('.login-page').css('display','block');
				// 	$('.signup-page').css('display','none');
				// });

				// $('.modal-trigger').leanModal({
				// 	dismissible: false,				
				// });

		    
			    var loader = '<div class="preloader-wrapper active">\
							    <div class="spinner-layer spinner-blue-only">\
							      <div class="circle-clipper left">\
							        <div class="circle"></div>\
							      </div><div class="gap-patch">\
							        <div class="circle"></div>\
							      </div><div class="circle-clipper">\
							        <div class="circle"></div>\
							      </div>\
							    </div>\
							  </div>';


			    $('.menu-list .menu-btn').click(function(e){
			    	e.preventDefault();
			    	var hide = $(this).data("hide");
			    	var scr = $( window ).width();	
			    	var height = $(window).height();
			     	var proHeaderHeight = $('.user-logo').height();
			     	var he = height-proHeaderHeight;	    	
			    	if(hide == true)
			    	{		    		
			    		if( scr > 600){    			
			    		   	$(this).parent().parent().animate({
					      		width: '30%'
					      	},800);
					      	
				     	}			     	
				     	else
				     	{			     		
				     		$(this).parent().parent().animate({
					      		width: '100%'
					      	},800);

				     	}
				     	$('#content-here').css({
				     		'height': height+'px'
				     	});
				      	$('#mobile li').css('display','none');
				      	$('#mobile .close').css('display','block');
				      	$('#mobile .user-logo').css('display','none');	      	

			      	}
			      	else
			      	{
				      	if( scr > 600){
			    			
					     	$(this).parent().parent().animate({
					      		width: '30%'
					      	},800);
				     	}			     	
				     	else
				     	{
				     		$(this).parent().parent().animate({
					      		width: '100%'
					      	},800);
				     	}
				     	
				     	$('#content-here').css({
				     		'height': '100%'
				     	});
						$('#mobile .user-logo').css({'position':'fixed','top':'0px'});
				      	$('#mobile li').css('display','none');
				      	$('#mobile .close').css('display','block');
				      	$('#mobile .user-logo').css('display','block');
				      	$('#mobile .user-logo #edit-btn').css('display','inline-block');
			      	}

			    	var url = $(this).attr('href');

			      	$.ajax({
					    url: url,
					    
					    beforeSend: function(data){
					    	$('#content-here').html(loader);
					    	
					    },
					    dataType: 'html',
					    success: function(data) {
					    	setTimeout(function(){
					    		$('#content-here').html(data);
					    	},1000);
					        
					        
					    }
					});

					$('#mobile #content-here').css('display','block');
			    });

			    $('#mobile .close').click(function(){
			    	$(this).parent().animate({
			      		width: 250
			      	},500);
			      	$('#mobile li').css('display','block');
			      	$(this).css('display','none');
			      	$('#mobile #content-here').css('display','none');
			      	$('#mobile .user-logo').css({'display':'block','position':'initial'});
			      	$('#mobile .user-logo #edit-btn').css('display','none');
			      	$('#content-here .profile .info-list #save-button').css('display','none');
			    });

			    $('#mobile .user-logo #edit-btn').click(function(){
			    	$('#content-here .profile .info-list input').removeAttr('readonly').eq(2).attr("type","text");
			    	$('#content-here .profile .info-list input').css('border-bottom','1px solid #9e9e9e');
			    	$('#content-here .profile .info-list #save-button').css('display','flex');
			    });
					
				$('#content').load('home.html');
				$('.preloader-wrapper').fadeOut('slow');
				$('.list-content .menu ul li a').click(function(){
					$('.preloader-wrapper').fadeIn('slow');
					 $('#content').fadeOut('slow');	
					loc = $(this).attr('href');
					setTimeout(function(){
					   $('#content').load(loc);
					   $('#content').fadeIn('slow');
					   $('.preloader-wrapper').fadeOut('slow');
					},700);
					filter = $(this).attr('rel');
					address = '.'+filter+'-'+'filter';
					
					$('.dropdown-filter,.additional-filter').find(address).addClass('filter-active').siblings().removeClass('filter-active');


				});		
				
				var navpos = $('.list-content .menu').offset();
				console.log(navpos.top);
				$(window).bind('scroll', function() {
					if ($(window).scrollTop() > navpos.top) {
						$('.list-content .menu').addClass('fixed');
					}
					else {
						$('.list-content .menu').removeClass('fixed');
					}
				});
		
				

			  	$('#tenants-drop a').click(function(){ 				
	 				$('.dropdown-tenants').slideToggle().toggleClass('active');
	 				$(this).find('.fa').toggleClass('rotate');
	 			});

	 			$('#landlord-drop a').click(function(){ 				
	 				$('.dropdown-landlord').slideToggle().toggleClass('active');
	 				$(this).find('.fa').toggleClass('rotate');
	 			});


	 			var slider = document.getElementById('range');
				noUiSlider.create(slider, {
						start: [20, 80],
						connect: true,
						step: 1,
						range: {
						'min': 0,
						'max': 100
					},
					format: wNumb({
						decimals: 0
					})
				});

				slider.noUiSlider.on('update', function( values, handle ) {
					if ( handle ) {
						$('#range-value').attr('data-max',values[1]);
						$('#range-value').attr('data-min',values[0]);
					} else {
						$('#range-value').attr('data-max',values[1]);
						$('#range-value').attr('data-min',values[0]);
					}
				});


				$("#filter-btn").click(function(){
					$(".filter-btn").slideToggle();       
				});


				$(window).resize(function(){
					container();
				});

				container();

				function container(){
					var listWidth = $(window).width();
					if(listWidth > 992 && listWidth < 1050){
						$('.details #list-container').removeClass('container').addClass('container-fluid');
					}
				}

		});


	</script>
	
</body>
</html>