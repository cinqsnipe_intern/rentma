<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id');
            $table->string('images')->nullable();
            $table->string('address')->nullable();
            $table->string('price')->nullable();
            $table->string('agent_name')->nullable();
            $table->string('description')->nullable();
            $table->integer('no_of_rooms')->nullable();
            $table->string('road_type')->nullable();
            $table->string('category')->nullable();
            $table->string('isnegotiable')->nullable();
            $table->string('owner_status')->nullable();
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
