<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        // Logic to authorize the request
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // Any other required laravel validations
        ];

        if (is_array($this->request->get('images'))):
            foreach ($this->request->get('images') as $key => $val):
                if($key == 0)  continue;
                $rules['images.' . $key] = 'required|image';
            endforeach;
        endif;
        return $rules;
    }
}