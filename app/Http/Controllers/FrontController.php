<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Detail;
use DB;

class FrontController extends Controller
{
    //
    public function home($category)
    { 
      // $lists=$this->Detail($category);
      $lists = $this->propertyList($category);
    	$show=DB::table('details')
              ->select('id','description','location','price','images','category')
              ->get();
        
        // dd($data);
        return view('fronts.list',compact('show','lists'));
    }


public function propertyList($category)
    {
        if (is_null($category))
        {
            $posts = DB::table('details')
                // ->join('users','users.id','=','posts.user_id')
                ->select('detail.id','details.description','details.location','details.price','details.images','details.category')
                ->orderBy('details.created_at','desc')
                ->get();
        }
        else{
            $posts = DB::table('details')
                // ->join('users','users.id','=','posts.user_id')
                ->select('details.id','details.description','details.location','details.price','details.images','details.category')
                
                ->where('details.category',$category)
                ->orderBy('details.created_at','desc')
                ->get();
        }
        return $posts;
    }
  }