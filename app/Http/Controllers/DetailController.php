<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Detail;
use App\Agent;
use DB;
class DetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=Detail::all();
        // dd($data);
        return view('details.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data=Detail::all();
        // dd($data);
        return view('details.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(request $request) {
       
       //image validation
        $input = $request->all();

      $rules = array(
    'images' => 'required',
     'images.*' => 'image|mimes:jpg,jpeg,png,PNG'
);

$validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=$file->getClientOriginalName();
                $file->move('image',$name);
                $images[]=$name;
            }
        }
        /*Insert your data*/

        Detail::insert( [
            // 'images'=>  "'".implode("|",$images)."'",
            'images'=>  implode("|",$images),
            'description' =>$input['description'],
            'agent_name' =>$input['agent_name'],
            'agent_id' =>$input['agent_id'],
            'price' =>$input['price'],
            'address' =>$input['address'],
            'isnegotiable' =>$input['isnegotiable'],
            'no_of_rooms' =>$input['no_of_rooms'],
            'road_type' =>$input['road_type'],
            'owner_status' =>$input['owner_status'],
            'location' =>$input['location'],
            'category' =>$input['category']
        ]);


        return redirect('/details');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // public function agentDetails($id)
    // {
    //     $agentdetail=DB::table('agents')
    //         ->select('id','images','name','phone','company','address')
    //         ->where('id',$id)
    //         ->get();
    //     $posts = DB::table('details')
    //         ->join('agents','agents.id','=','details.agent_id')
    //         ->select('id','agent_id','images','address', 'price', 'agent_name', 'description', 'no_of_rooms','road_type',  'category','isnegotiable'    'owner_status', 'location ');
           
    //        ->where('details.agent_id',$id)
    //         ->get();
    //     $agents=DB::table('agents')
    //         ->select('id','images','name','phone','address','company')
    //         ->get();
    //          return view('frontend.agentDetails')->with(compact('notices','agentdetail','posts','agents'));
    // }

    public function show($id)
    {
        
        $agentdetail=DB::table('details')
            ->join('agents','agents.id','=','details.agent_id')
            ->select('agents.id','agents.images','agents.name','agents.phone','agents.company')
            ->where('details.id',$id)
            ->get();
            // dd($agentdetail);
        // $agent=DB::table('agents')
        //            ->select('agents.images')
        //            ->get();
        $show=DB::table('details')
              ->select('id','description','location','price','images')
              ->get();
        $data=Detail::find($id);
        return view('details.show',compact('data','show','agentdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = Detail::findorfail($id);
        $data->update($request->all());
        return redirect('details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
