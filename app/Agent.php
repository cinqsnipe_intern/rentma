<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    //
    Protected $fillable= [
             'id','agent_name','address','phone','images','company','introduction','specialities','regions_covered','reviews',
    ];
}
