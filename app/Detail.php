<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    //
     // protected $table = 'details';
    protected $fillable = [
        'id','agent_id', 'agent_name', 'price','location','address','description','images','isnegotiable','no_of_rooms','road_type','owner_status','category',
    ];
}
